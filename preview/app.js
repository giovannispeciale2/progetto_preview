var express = require('express');
var session = require('express-session');
var app = express();
var mongoose = require('mongoose');
var User = require('./src/models/userModel');
var Category = require('./src/models/categoryModel');
var Image = require('./src/models/imgModel');
var Work = require('./src/models/workModel');
var Review = require('./src/models/reviewModel');
var Notification = require('./src/models/notificationModel');
var bodyParser = require('body-parser');
var schedule = require('node-schedule');
var dayRankBadge = require('./src/controllers/dailyBadgeController');

var myArgs = process.argv.slice(2);
switch(myArgs[0]){
  case 'local':
    mongoose.connect('mongodb://localhost/dbpreview', {poolSize: 10, useNewUrlParser: true, useFindAndModify: false });
    break;
  default:
    mongoose.connect('mongodb+srv://adminpreview:adminpreview@cluster0-f1svd.mongodb.net/dbpreview?retryWrites=true&w=majority',
                    {poolSize: 10, useNewUrlParser: true, useFindAndModify: false });
}

//mongoose.connect('mongodb://localhost/dbpreview', {poolSize: 10, useNewUrlParser: true, useFindAndModify: false });
//mongoose.connect('mongodb+srv://adminpreview:adminpreview@cluster0-f1svd.mongodb.net/dbpreview?retryWrites=true&w=majority', {poolSize: 10, useNewUrlParser: true, useFindAndModify: false });
app.use(session ({
  secret: 'san giovese',
  resave: true,
  saveUninitialized: false,
  cookie: { secure: false }
}));

app.use(bodyParser.urlencoded({ limit: '50mb',  extended: true }));
app.use(bodyParser.json({limit: '50mb'}));
app.use('/static', express.static(__dirname + '/public'));

var path = require('path');
global.appRoot = path.resolve(__dirname);
global.uploadPath = path.resolve(__dirname + '/upload');
global.routerPath = path.resolve(__dirname + '/public');

var routes = require('./src/routes/previewRoutes');
routes(app);

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

// var j = schedule.scheduleJob('0 0 0 * * *', dayRankBadge.bestBadge()); // 0s 0m 0h *d *dmo *dw la funzione viene richiamata ogni giorno a mezzanotte

var t = setInterval(runFunction, 10000);

function runFunction() {
  dayRankBadge.bestBadge();
}

app.listen(3000, function () {
  console.log('Node API server started on port 3000!');
});
