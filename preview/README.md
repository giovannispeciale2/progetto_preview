Run with local DB, type:
  node app.js local

Run with remote DB, type:
  node app.js

Build Dockerfile:
  docker build -t preview .
  
Run Dockerfile:
  docker run -p 80:3000 preview
