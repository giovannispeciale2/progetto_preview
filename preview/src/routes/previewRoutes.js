module.exports = function(app) {
	var usersController = require('../controllers/usersController');
	var categoriesController = require('../controllers/categoriesController');
	var imgController = require('../controllers/imgController');
	var reviewController = require('../controllers/reviewController');
	var worksController = require('../controllers/workController');
	// var quotesController = require('../controllers/quotesController');
	var pageController = require('../controllers/pageController');
	var notifyController = require('../controllers/notifyController');

	app.route('/')
	.get(pageController.show_page);

////////////////////////// CATEGORY  /////////////////////////////////
	app.route('/api/categories')
	.get(categoriesController.list_categories);

////////////////////////// REVIEW  /////////////////////////////////
	app.route('/api/review/:id')
	.get(reviewController.searchReviewFromId);

	app.route('/api/reviews/deleteReview')
	.post(reviewController.deleteReview);

	app.route('/api/reviews/user/:user')
	.get(reviewController.getUserReviews);

	app.route('/api/reviews/work')
	.get(reviewController.getWorkReviews);

	app.route('/api/reviews/rate')
	.get(reviewController.addRate);

	app.route('/api/reviews')
	.get(reviewController.searchReviews)
	.post(reviewController.addReview);

	app.route('/api/reviews/most_recent_reviews')
	.get(reviewController.mostRecentReviews);

	app.route('/api/reviews/best_rated_reviews')
	.get(reviewController.bestRatedReviews);

////////////////////////// WORK  //////////////////////////////////
	app.route('/api/works')
	.get(worksController.searchWorks)
	.post(worksController.addWork);

	app.route('/api/works/deleteWork')
	.post(worksController.deleteWork);

	app.route('/api/work/edit')
	.post(worksController.editDataWork);

	app.route('/api/works/:id')
	.get(worksController.searchWorkFromId);

////////////////////////// USER  //////////////////////////////////
	app.route('/api/users')
	.post(usersController.create_user);

	app.route('/api/users/:id')
	.get(usersController.searchUserFromId);

	app.route('/api/login')
	.get(usersController.create_session);

	app.route('/api/user/updatePoints')
	.get(usersController.updatePoints);

	app.route('/api/user/updateRates')
	.get(usersController.updateRates);

	app.route('/api/user/edit')
	.post(usersController.editDataUser);

	app.route('/api/user/ban')
	.post(usersController.banUser);

	app.route('/api/userRanking')
	.get(usersController.getRank);

	app.route('/api/forgot')
	.get(usersController.forgot_password);

	app.route('/api/other/:name')
	.get(usersController.getOther);

	///////////////////////// IMAGES ////////////////////////////
	const multer = require('multer');

	app.route('/api/imgRender')
	.get(imgController.img_render);

	const upload = multer({
		dest: uploadPath
	});

	app.route('/api/imgUpload')
 	.post(upload.single('file'), imgController.save_img);


////////////////////////// NOTIFY ////////////////////////////////
	app.route('/api/notify/notification/:id')
	.get(notifyController.getAll);

	app.route('/api/notify/new/:id')
	.get(notifyController.getNew);

	app.route('/api/notify/seen/:id')
	.get(notifyController.seen);
///////////////////////////////////////////////////////////////
	app.use(pageController.show_page);
};
