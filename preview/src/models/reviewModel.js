var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReviewSchema = new Schema({
  user: {
    type: String,
	},
  work: {
    type: String,
  },
  date: {
    type: Date,
  },
  title: {
    type: String,
  },
  text: {
    type: String
  },
  avg: {
    type: Number,
    default: 0,
  },
  points: {
    type: Number,
    default: 0,
  },
  rates: {
    type: Array,
    default: []
  },
},
{versionKey: false}
);

module.exports = mongoose.model('Reviews', ReviewSchema);
