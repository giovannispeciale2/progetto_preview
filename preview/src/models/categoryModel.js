var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CategorySchema = new Schema({
  name: {
    type: String,
	},
  genres: {
    type: Array,
  },
  img: {
    type: String,
  }
},
{versionKey: false}
);

module.exports = mongoose.model('Categories', CategorySchema);
