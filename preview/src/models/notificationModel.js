var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NotificationSchema = new Schema({
  seen:{
    type: Boolean,
    default: false,
  },
  user:{
    type: String,
    required: 'User is required'
  },
  review:{
    type: String,
    default: ""
  },
  reviewId:{
    type: String,
    default: ""
  },
  description:{
    type: String,
    required: 'Description is required'
  },
  badge:{
    type: String,
    default: ""
  }
},
{versionKey: false}
);

module.exports = mongoose.model('Notifications', NotificationSchema);
