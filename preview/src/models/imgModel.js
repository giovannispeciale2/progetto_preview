var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ImageSchema = new Schema({
  url: {
    type: String,
	},
  public_id: {
    type: String,
  },
});

module.exports = mongoose.model('Images', ImageSchema);
