var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  nickname: {
    type: String,
    required: 'Nickname is required'
	},
  password: {
    type: String,
    required: 'Password is required'
  },
  mail: {
    type: String,
    required: 'Email is required'
  },
  sex: {
    type: String,
    required: 'Gender is required'
  },
  img: {
    type: String,
  },
  admin: {
    type: Boolean,
    default: false
  },
  points: {
    type: Number,
    default: 0
  },
  bonus: {
    type: Number,
    default: 0
  },
  votes: {
    type: Number,
    default: 0
  },
  badges: {
    type: Array,
    default: []
  }
},
{versionKey: false}
);

module.exports = mongoose.model('Users', UserSchema);
