var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WorkSchema = new Schema({
  title: {
    type: String,
	},
  author: {
    type: String,
  },
  date: {
    type: Date,
  },
  img: {
    type: String,
  },
  category: {
    type: String,
  },
  genre: {
    type: String,
  },
},
{versionKey: false}
);

module.exports = mongoose.model('Works', WorkSchema);