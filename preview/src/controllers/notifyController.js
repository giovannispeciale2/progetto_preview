var mongoose = require('mongoose');
var Notification = mongoose.model('Notifications');

exports.getAll = function(req, res){
  var user = req.params.id;
	Notification.find({'user': user}, (err, result) => {
		if(err)
			res.send(err);
		else
			res.json(result);
	});
};

exports.getNew = function(req, res){
  var user = req.params.id;
  Notification.find({'user': user}, (err, result) => {
    if(err)
      res.send(err);
    else
      res.json(result);
  });
}

exports.seen = function(req, res){
  var notification = req.params.id;
  Notification.findByIdAndUpdate(notification, {'seen': true}, (err, result) => {
    if(err)
      res.send(err);
    else
      res.send(result);
  });
}
