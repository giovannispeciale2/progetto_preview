var rank = require('./usersController');
var mongoose = require('mongoose');
var User = mongoose.model('Users');
var Notification = mongoose.model('Notifications');

exports.bestBadge = async function(){
  var result = await rank.rankForBadge();
  var position = 1;
  result = result.slice(0, 101);
  result.forEach((person) => {
    if(position <= 3)
      badge(person, 'static/images/Badges/3rdClassified.png', 50, "Congratulazioni, sei il terzo classificato. Guadagni un Badge e 50 punti");
    if(position <= 2)
      badge(person, 'static/images/Badges/2ndClassified.png', 75, "Congratulazioni, sei il secondo classificato. Guadagni un Badge e 75 punti");
    if(position === 1)
      badge(person, 'static/images/Badges/1stClassified.png', 100, "Congratulazioni, sei il primo classificato. Guadagni un Badge e 100 punti");
    if(position <= 10)
      badge(person, 'static/images/Badges/BadgeTop10.png', 10, "Congratulazioni, sei tra i Top 10. Guadagni un Badge e 10 punti");
    if(position <= 100)
      badge(person, 'static/images/Badges/BadgeTop100.png', 5, "Congratulazioni, sei tra i Top 100. Guadagni un Badge e 5 punti");
    position+= 1;
  })
}

function badge(user, badgeName, val, message) {
  User.findById(user._id, (err, userResult) => {
    if(err) {
      return err;
    } else {
      if(!userResult.badges.includes(badgeName)) {
        User.updateOne({'nickname' : user.nickname},	{$push: {'badges': badgeName}}, (err, end) =>{
      		if(err) {
      			return err;
          } else {
      			User.updateOne({'nickname' : user.nickname}, {$inc: {'bonus': val, 'points': val}}, (err, end) =>{
      				if(err) {
      					return err;
              } else {
      					notify(user.nickname, badgeName, val, message);
      					return end;
      					}
            });
          }
      	});
      }
    }
  });
}

function notify(user, badgeName, val, message){
	User.findOne({'nickname': user}, (err, userRes) => {
    var notification = new Notification();
  	notification.description = message;
  	notification.badge = badgeName;
		notification.user = userRes._id;
		notification.save((err, result) => {
			if(err)
				console.log(err);
		});
	})
}
