var mongoose = require('mongoose');
var User = mongoose.model('Users');
var Notification = mongoose.model('Notifications');
var Review = mongoose.model('Reviews');
var Work = mongoose.model('Works');
var bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

/////////////// RECUPERO PASSWORD  ////////////////
exports.forgot_password = function(req, res) {
	User.findOne({'mail': req.query.mail}, function(err, user) {
		if(user === null) {
			res.json({"error": "Email inesistente. Inserisci la tua mail di registrazione."});
		} else {
			var newPassword = (Math.round(Math.random()* 100) + 1) * 666;
			mail(req.query.mail, "Ripristino Password", "la tua nuova password è " + newPassword);
			const saltRounds = 10;
			bcrypt.hash(newPassword + "", saltRounds)
				.then(cript => User.findByIdAndUpdate(user._id, {$set: {'password': cript}}, (err, end) => {
					if(err)
						console.log(err);
				}));
			res.json({"success": "Email di recupero inviata con successo."});
		}
	});
}

function mail(to, subject, text){
	var transporter = nodemailer.createTransport({
			service: 'gmail',
			auth: {
				user: 'previewnonrispondere@gmail.com',
				pass: 'PReview-123'
			}
    });

    var mailOptions = {
      from: 'previewnonrispondere@gmail.com',
      to: to,
      subject: subject,
      text: text
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error)
        console.log(error);
    });
}

//////////////// SESSIONE ////////////////////////
exports.create_session = function(req, res) {
	if(!req.query.nickname || !req.query.password){
    res.send("Inserisci le credenziali!");
  }
  else
    User.findOne({'nickname': req.query.nickname}, (err, userResult) => {
	    if(!userResult) res.json({"error": "Error in login"});
		else{
			bcrypt.compare(req.query.password, userResult.password, (err, cryptResult) => {
				if(err) res.send("error in password control");
				if(cryptResult === false) res.json({"error": "Error in login"});
				else{
					res.status(200);
					res.send(userResult._id);
				}
			});
		}
    });
};

//////////// REGISTRAZIONE USER  //////////////////////////////////////////////
exports.create_user = function(req, res) {
	controlNickname(req.body.nickname, function(result) {
		if(result){
			controlEmail(req.body.mail, function(result) {
				if(result) {
					var new_user = new User(req.body);
					const saltRounds = 10;
					const myPlaintextPassword = new_user.password;
					bcrypt.hash(myPlaintextPassword, saltRounds)
						.then( function(hash) { new_user.password = hash; })
						.then(() => {
							if(new_user.img == "")
								if(new_user.sex == "Maschio")
									new_user.img = "/static/images/img_male_avatar.png";
								else if(new_user.sex == "Femmina")
									new_user.img = "/static/images/img_female_avatar.png";
							new_user.save(function(err, user) {
							if (err) res.send(err);
							res.status(201).json(user);
						});
					});
				}
        else res.json({error:"Email Already Exists"});
			});
		}
    else res.json({error:"Nickname Already Exists"});
	});
};

function controlNickname(nickname, callback) {
	User.findOne({'nickname': nickname}, function(err,res) {
		if(!res)  callback(true);
		else  callback(false);
	});
}

function controlEmail(email, callback) {
	User.findOne({'mail': email}, function(err,res) {
		if(!res) callback(true);
		else  callback(false);
	});
}

//////////// USER INFO ////////////////////////////////////////////////////
exports.searchUserFromId = function(req, res) {
	User.findById(req.params.id, function(err, user) {
		if (err)
			res.send(err);
		else{
			if(user === null){
				res.status(404).send({
					description: 'User not found'
				});
			}
			else{
				res.json(user);
			}
		}
	});
};

//////////////////// MODIFICA DATI //////////////////////////////////////////
exports.editDataUser = function(req, res) {
	const saltRounds = 10;

	User.findById(req.body.idU, (err, userResult) => {
		if(!userResult) res.json({"error": "Error in server"});
		else{
			bcrypt.compare(req.body.oldPsw, userResult.password, (err, cryptResult) => {
				if(err) res.send("error in password control");
				if(cryptResult === false) res.json({"error": "Vecchia password non corrispondente! Riprova."});//res.json({"error": "Old password not match"});
				else if(req.body.oldPsw == req.body.password)
					res.send({"error": "Non puoi inserire la stessa password"});
				else {
					if(req.body.password == undefined || req.body.password == "")
						User.findByIdAndUpdate(req.body.idU, {$set: {'nickname': req.body.name, 'mail': req.body.email, 'img': req.body.img}}, (err, end) => {
							if(err)
								res.send(err);
							else
								res.send(end);
						});
					else {
						bcrypt.hash(req.body.password, saltRounds)
						.then(hash => {
							User.findByIdAndUpdate(req.body.idU, {$set: {'nickname': req.body.name, 'password': hash, 'mail': req.body.email, 'img': req.body.img}}, (err, end) => {
								if(err)
									res.send(err);
								else
									res.send(end);
							});
						});
					}
				}
			});
		}
	});
}

//////////////////// MODIFICA PUNTEGGIO //////////////////////////////////////////
exports.updatePoints = function(req, res){
	User.find({'nickname' : req.query.name}, function(err, user){
		if(err)
			res.send(err);
		else
			User.updateOne({'nickname' : req.query.name}, {$inc: {'points': req.query.grade}}, (err, end)=>{
				if(err)
					res.send(err);
				else
					res.send(end);
			});
	});
}

///////////////////////////// BANNA UTENTE ////////////////////////////////////////
exports.banUser = function(req, res) {
	var idUser = req.body.idU.id;
	User.findById(idUser, (err, user) => {
		if(err)
			res.send(err);
		else {
			mail(user.mail, "Bannato da (p)Review",
					"Ciao " + user.nickname + ". Il tuo account in '(p)Review' è stato rimosso a causa di contenuti poco chiari creati e/o valutazioni inopportune. Puoi sempre crearne uno nuovo con l'auspicio di migliorare la tua condotta.");
			User.deleteOne({'_id': idUser}, (err, resultU) => {
				if(err)
					res.send(err);
				else {
					console.log("Cancellato utente: " + user.nickname);
					Review.deleteMany({'user': idUser}, (err, resultR) => {
						if(err)
							res.send(err);
						else {
							console.log("Cancellate review di: " + user.nickname);
							var reviewsWithBanned = [];
							Review.find({'rates': {$elemMatch: {'voter': idUser}}}, (err, reviewsBan) => {
								if(err)
									res.send(err);
								else {
									reviewsBan.forEach(reviewBan => {
										reviewsWithBanned.push(reviewBan);
									});
									Review.updateMany({}, {$pull: {'rates': {'voter': idUser}}}, (err, resultQ) => {
										console.log("Cancellate votazioni di: " + user.nickname);
										Notification.deleteMany({'user': idUser}, (err, resultN) => {
											console.log("Cancellate notifiche di: " + user.nickname);
											reviewsWithBanned.forEach(rev => {
												Review.findById(rev._id, (err, rev2) => {
													if(err)
														res.send(err);
													else {
														var tot = 0;
										        var avg = 0;
										        if(rev2.rates.length > 0) {
										          rev2.rates.forEach(rate => tot += Number(rate.grade));
										          avg = (tot / rev2.rates.length).toFixed(2);
															// console.log(rev2.retes.length + ", " + avg);
										        }
										        Review.findByIdAndUpdate(rev2._id, {$set: {'avg': avg, 'points': tot}}, (err, risultato) => {
															if(err)
																res.send(err);
														});
													}
												});
											});
										});
									});
								}
							});
						}
					});
					console.log("Bannato con successo!");
					res.send(resultU);
				}
			});
		}
	});
}

///////////////////// MODIFICA VOTAZIONE /////////////////////////////////////////
exports.updateRates = function(req, res){
	User.findByIdAndUpdate(req.query.id, {$inc: {'votes': 1}}, (err, end)=>{
		if(err)
			res.send(err);
		else{
			User.findById(req.query.id, (err, user)=>{
				if(err)
					res.send(err);
				else {
					if(user.votes === 10)
						res.status(badge(user.nickname, 'static/images/Badges/BadgeGrade1.png', 10));
					else if(user.votes === 50)
						res.status(badge(user.nickname, 'static/images/Badges/BadgeGrade2.png', 50));
					else if(user.votes === 100)
						res.status(badge(user.nickname, 'static/images/Badges/BadgeGrade3.png', 100));
					else if(user.votes === 200)
						res.status(badge(user.nickname, 'static/images/Badges/BadgeGrade4.png', 200));
				}
			});
		}
	});
}

function badge(user, badgeName, val){
	User.updateOne({'nickname' : user},	{$push: {'badges': badgeName}}, (err, end) =>{
		if(err)
			return err;
		else
			User.updateOne({'nickname' : user}, {$inc: {'bonus': val, 'points': val}}, (err, end) =>{
				if(err)
					return err;
				else {
					notify(user, badgeName, val);
					return end;
					}
			});
	});
}

function notify(user, badgeName, val){
	var message = "Congratulazioni, hai valutato " + val + " recensioni di altri utenti. Meriti un badge e " + val +  "punti!";
	var notification = new Notification();
	notification.description = message;
	notification.review = "";
	notification.reviewId = "";
	notification.badge = badgeName;
	User.findOne({'nickname': user}, (err, userRes) => {
		notification.user = userRes._id;
		notification.save((err, result) => {
			if(err)
				return err;
			else {
				return result;
			}
		});
	})
}

//////////////////////////////// RANKING /////////////////////////////////
exports.getRank = function(req, res){
	User.find({}, (err, result)=> {
		if (err)
			res.send(err);
		else
			res.json(result);
	}).sort({'points': -1});
}

exports.rankForBadge = function(){
	return new Promise((resolve, reject) => {
		User.find({}, (err, result)=> {
			if (err)
				reject();
			else
				resolve(result);
		}).sort({'points': -1});
	});
}

////////////////// OTHER ////////////////////////////////////////
exports.getOther = function(req, res){
	var nickname = req.params.name;
	User.findOne({'nickname': nickname}, (err, result) => {
		if(err)
			res.send(err);
		else
			result? res.send(result._id) : res.send(null);
	});
}
