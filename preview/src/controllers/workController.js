var mongoose = require('mongoose');
var Work = mongoose.model('Works');
var Review = mongoose.model('Reviews');

exports.searchWorks = function(req, res) {
	var regExp1 = new RegExp("^" + req.query.wordToSearch + ".*$");
	var regExp2 = new RegExp("^.*\\s" + req.query.wordToSearch + ".*$");
	Work.find({
		$or: [
			{'title' : {$regex: regExp1, $options: 'i'} },
			{'title' : {$regex: regExp2, $options: 'i'} },
		]}, function(err, results) {
		if(results) {
			res.json(results);
		} else
			res.json(err);
	});
};

exports.editDataWork = function(req, res) {
	Work.findByIdAndUpdate(req.body.idW, {$set: {
		'title': req.body.title,
		'author': req.body.author,
		'img': req.body.img,
		'date': req.body.date,
		'category': req.body.category,
		'genre': req.body.genre,
	}}, (err, end) => {
		if(err)
			res.send(err);
		else
			res.send(end);
	});
};

exports.addWork = function(req, res) {
    var new_work = new Work(req.body);
    var alreadyExisting = false;
    var regExp = new RegExp(req.body.title);
    Work.findOne({'title' : {$regex: regExp, $options: 'i'}}, function(err, result) {
        if(result)
            if(equalDates(new Date(req.body.date), result.date))
                alreadyExisting = true;
    }).then(response => {
        if(!alreadyExisting) {
						if(new_work.img == "")
							new_work.img = "/static/images/noWorkImageAvailable.png";
            new_work.save(function(err, work) {
                if (err)
                    res.send(err);
                res.status(201).json(work);
            });
        } else
            res.json({'Error': 'Work already existing'});
    });
};

function equalDates(date1, date2) {
    return (date1.getFullYear() === date2.getFullYear()) &&
            (date1.getMonth() === date2.getMonth()) &&
            (date1.getDate() == date2.getDate());
}

exports.searchWorkFromId = function(req, res) {
	Work.findById(req.params.id, function(err, work) {
		if (err)
			res.send(err);
		else {
			if(work==null){
				res.status(404).send({
					description: 'Work not found'
				});
			}
			else{
				res.json(work);
			}
		}
	});
};

exports.deleteWork = function(req, res) {
	Work.deleteOne({'_id': req.body.idW}, function(err, work) {
		if(err)
			res.send(err);
		else
			Review.deleteMany({'work': req.body.idW}, function(err, review) {
				if(err)
					res.send(err);
				else
					res.json(work);
			});
	});
};
