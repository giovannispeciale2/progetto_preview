const crypto = require("crypto");
const fs = require("fs");
const path = require("path");

exports.save_img = function(req, res){

	crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        var imgName = "/" + buf.toString('hex') + ".png";
		var targetPath = path.join(uploadPath, imgName);
		var image = req.body.params.img;

		var data = image.replace(/^data:image\/\w+;base64,/, '');

		fs.writeFile(targetPath, data, {encoding: 'base64'}, function(err) {
			var pathForUrl = imgName.replace("/", "?imgPath=%2F")
			var pathToSave = "/api/imgRender" + pathForUrl;
			res.send(pathToSave);
		});
	})
};

function decodeBase64Image(dataString) {
	var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
	  response = {};

	if (matches.length !== 3) {
	  return new Error('Invalid input string');
	}

	response.type = matches[1];
	response.data = Buffer.from(matches[2], 'base64');

	return response;
}

exports.img_render = function(req, res) {
	res.sendFile(uploadPath + req.query.imgPath);
}
