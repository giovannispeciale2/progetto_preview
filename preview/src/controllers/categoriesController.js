var mongoose = require('mongoose');
var Category = mongoose.model('Categories');

exports.list_categories = function(req,res) {
	Category.find({},null, {sort: {name: 1}}, function(err, categoryList) {
		if(err)
			res.send(err);
		else
			res.json(categoryList);
	});
};

exports.category_data = function(req, res) {
	Category.find({'name': req.params.category}, function(err, result){
		if(err)
			res.send(err);
		else
			res.json(result);
	});
}
