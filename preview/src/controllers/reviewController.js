var mongoose = require('mongoose');
var Review = mongoose.model('Reviews');
var User = mongoose.model('Users');
var Work = mongoose.model('Works');
var Notification = mongoose.model('Notifications');

exports.addRate = function(req, res) {
  Review.findById(req.query.idR, (err, review)=>{
    if(err)
      res.send(err);
    else {
			User.findById(req.query.idU, (err, user) => {
				notifyVote("La tua recensione " + review.title + " ha ricevuto voto " + req.query.grade + " da " + user.nickname
                    + ". Hai ottenuto " + req.query.grade + " punti.", review.title, req.query.idR, review.user);
	      var index = review.rates.findIndex(rate =>rate.voter == req.query.idU);
	      if(index === -1)
	        Review.updateOne({
						'title': review.title,
						'work': review.work,
						'user': review.user
					},
	        {$push: {'rates':  {'voter': req.query.idU, 'grade': req.query.grade}}},
	        (err, end) => {
						if(err)
							res.send(err);
	          else {
							controlRateBadge(req.query.idR, review.user);
	            Review.updateOne({
								'title' : review.title,
								'work': review.work,
								'user': review.user
							},
							{$inc: {'points':  req.query.grade}},
	            (err, end) => {
								if(err)
									res.send(err);
	              else
									avgUpdate(req.query.idR, {dif: -10})
									.then(result => res.json(result));
	            });
	          }
	        });
	      else {
	        var dif = req.query.grade - review.rates[index].grade;
	        Review.updateOne({
						'title' : review.title,
						'work': review.work,
						'user': review.user
					},
	        {$set: {'rates.$[elem].grade': req.query.grade}},
	        {multi: true, arrayFilters: [{'elem.voter': {$gte: req.query.idU}}]},
	        (err, end) => {
						if(err)
							res.send(err);
						else {
							controlRateBadge(req.query.idR, review.user);
	            Review.updateOne({
								'title' : review.title,
								'work': review.work,
								'user': review.user
							},
	            {$inc: {'points':  dif}},
	            (err, end) => {
								if(err)
									res.send(err);
	              else
									avgUpdate(req.query.idR, {dif: dif})
									.then(result => res.json(result));
	          	});
						}
	        });
	      }
			});
    }
  });
};

function notifyVote(message, review, reviewId, user) {
    var notification = new Notification();
    notification.description = message;
    notification.review = review;
    notification.reviewId = reviewId;
    notification.user = user;
    notification.save();
}

function controlRateBadge(review, reviewer){
  Review.findById(review, (err, res)=>{
    if(err)
      console.log("Errore controllo badge");
    else {
      var filter = res.rates.filter((rate) => rate.grade >= 3);
      var len = filter.length;
      var msg1 = "La tua recensione è stata votata positivamente da ";
      if(len === 10) {
        var msg2 = " altri utenti!! Guadagni un BADGE e 10 punti.";
        badge(reviewer, 'static/images/Badges/Badge_10_Positive.png', 10, res.title, "", msg1, msg2);
      }
      else if(len === 50) {
        var msg2 = " altri utenti!! Guadagni un BADGE e 50 punti.";
        badge(reviewer, 'static/images/Badges/Badge_50_Positive.png', 50, res.title, "", msg1, msg2);
      }
      else if(len === 100) {
        var msg2 = " altri utenti!! Guadagni un BADGE e 100 punti.";
        badge(reviewer, 'static/images/Badges/Badge_100_Positive.png', 100, res.title, "", msg1, msg2);
      }
      else if(len === 200) {
        var msg2 = " altri utenti!! Guadagni un BADGE e 200 punti.";
        badge(reviewer, 'static/images/Badges/Badge_200_Positive.png', 200, res.title, "", msg1, msg2);
      }
    }
  });
}

function badge(user, badgeName, val, review, reviewId, msg1, msg2){
  var message = msg1 + val + msg2;
  Notification.findOne({'user': user, 'description': message}, (err, earned) => {
    if(err)
      return err;
    else if(!earned) {
      User.findByIdAndUpdate(user, {$push: {'badges': badgeName}}, (err, end) =>{
    		if(err)
    			console.log("Errore assegnazione badge");
    		else
    			User.findByIdAndUpdate(user, {$inc: {'bonus': val, 'points': val}}, (err, end) =>{
    				if(err)
    					console.log("Errore punteggio badge");
            else {
              var notification = new Notification();
              notification.description = message;
              notification.badge = badgeName;
              notification.review = review;
              notification.reviewId = reviewId;
              notification.user = user;
              notification.save((err, result) => {
                if(err)
                  console.log("Errore nel salvataggio della notifica");
              });
            }
    			});
    	});
    }
  });
}

function avgUpdate(review, returnVar){
	return new Promise((resolve, reject) => {
		Review.findById(review, (err, rev) => {
			var gradeArray = rev.rates.map(rate => {return parseFloat(rate.grade);});
			var media = gradeArray.reduce((accumulator, rate) => {
				return accumulator += rate;
			});
			media /= rev.rates.length;
			Review.findByIdAndUpdate(review, {$set: {'avg': media}}, (err, end) => {
				if(err)
					reject(err);
				else
					resolve(returnVar);
			});
		});
	});
}

exports.addReview = function(req, res) {
  var new_review = new Review(req.body);
	new_review.save(function(err, review) {
		if (err)
			res.send(err);
    controlReviewBadge(review, review.user)
    res.status(201).json(review);
	});
};

function getCategory(idW){
	return new Promise((resolve, reject) => {
    Work.findById(idW, (err, work) => {
  		if(err)
  			reject(err);
  		else
  			resolve(work.category);
  	});
  });
}

function controlReviewBadge(review, reviewer){
  Review.find({'user': reviewer}, (err, res) => {
    var len = res.length;
    var msg1 = "Ricevi un badge e ";
    var msg2 = "";
    if(len === 1){
      msg2 = " punti per la tua prima recensione!";
      badge(reviewer, 'static/images/Badges/BadgeLevel1.png', 10, res.title, "", msg1, msg2);
    }
    else if(len === 10){
      msg2 = " punti per la tua decima recensione!";
      badge(reviewer, 'static/images/Badges/BadgeLevel2.png', 50, res.title, "", msg1, msg2);
    }
    else if(len === 50){
      msg2 = " punti per la tua cinquantesima recensione!";
      badge(reviewer, 'static/images/Badges/BadgeLevel3.png', 100, res.title, "", msg1, msg2);
    }
    else if(len === 100){
      msg2 = " punti per la tua centesima recensione!";
      badge(reviewer, 'static/images/Badges/BadgeLevel4.png', 200, res.title, "", msg1, msg2);
    }
    let reviewCategory = getCategory(review.work).then(response => {
      let promise = res.map((rev) => getCategory(rev.work));
      Promise.all(promise)
      .then(resp => {
        if(resp.filter(cat => cat === response).length === 1){
          var categoryName = response.replace(" ", "_");
          categoryName = categoryName.replace("/", "_")
          var badgeName = "static/images/Badges/" + categoryName + "Badge"+ ".png";
          var msg1 = "Ricevi un badge e "
          var msg2 = " punti per la tua prima recensione nella categoria " + response + "!"
          badge(reviewer, badgeName, 5, "", "", msg1, msg2);
        }
      });
    });
  });
}

exports.searchReviews = function(req, res) {
	if(req.query._id){
		Review.find({'work' : req.query._id }, (err, results) => {
			if(results)
				res.json(results);
			else
				res.json(err);
		});
	}
	else {
		if(req.query.idR)
			Review.find({'_id': req.query.idR}, (err, results) => {
			if(results)
				res.json(results);
			else
				res.json(err);
			});
		else
			Review.find((err, results) => {
				if(results)
					res.json(results);
				else
					res.json(err);
			});
	}
};

exports.searchReviewFromId = function(req, res) {
	Review.findById(req.params.id, function(err, review) {
		if (err)
			res.send(err);
		else{
			if(review==null){
				res.status(404).send({
					description: 'Recensione non trovata'
				});
			}
			else{
				res.json(review);
			}
		}
	});
};

exports.deleteReview = function(req, res) {
	Review.deleteOne({'_id': req.body.idR}, function(err, review) {
		if(err)
			res.send(err);
		else {
			res.json(review);
		}
	});
};

exports.getWorkReviews = function(req, res) {
	if(req.query.idW){
		Review.find({'work' : req.query.idW }, (err, results) => {
			if(err)
				res.send(err);
			else
				res.send(results);
		}).sort({'avg': -1});
	}
};

exports.getUserReviews = function(req, res) {
	if(req.params.user){
		Review.find({'user' : req.params.user }, (err, results) => {
			if(results)
				res.json(results);
			else
				res.json(err);
		}).sort({'date': -1});
	}
};

exports.mostRecentReviews = function(req, res) {
	Review.find({}, (err, results) => {
		if(results)
			res.json(results);
		else
			res.json(err);
	}).sort({'date': -1});
};

exports.bestRatedReviews = function(req, res) {
	Review.find({}, (err, results) => {
		if(results)
			res.json(results);
		else
			res.json(err);
	}).sort({'avg': -1});
};

exports.avg = function(req, res){
  Review.find({}, (err, result) => {
    result.forEach(rev => {
      if(err)
        res.send(err);
      else{
        var tot = 0;
        var avg = 0;
        if(rev.rates.length > 0){
          rev.rates.forEach(rate => tot += Number(rate.grade));
          avg = (tot / rev.rates.length).toFixed(2);
        }
        Review.findByIdAndUpdate(rev._id, {$set: {'avg': avg, 'points': tot}}, (err, upd) => {
          if(err)
            res.send(err);
        });
      }
    });
  });
}
