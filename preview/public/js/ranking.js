const Rank = {
  template: `
  <div class="body">
    <headerbar :key="headerKey"></headerbar>
    <main>
      <div class="main">
        <shortbar></shortbar>
        <div class="main-body">
          <!-- Rankings -->
          <div class="ranking">
            <div class="ranking-users-title-container">
              <div class="ranking-users-title">
                Classifica Utenti Più Attivi
              </div>
            </div>
            <table class="ranking-users">
              <tr class="ranking-users-rowHead">
                <th class="ranking-position">#</th>
                <th class="ranking-nick">Utente</th>
                <th class="ranking-halfpti">Punteggio Review</th>
                <th class="ranking-halfpti">Punteggio Bonus</th>
                <th class="ranking-total">Punteggio Totale</th>
              </tr>
              <tr class="ranking-users-row" v-for= "result in rank.slice(0, 15)" v-bind:style= "getColor(result)" @click.prevent="goToProfile(result)">
                <td class="ranking-position"> {{getPosition(result)}} </td>
                <td class="ranking-nick">
                  <!--<router-link :to="{ name:'others', params: {username: result.nickname}}">-->
                    {{result.nickname}}
                  <!--</router-link>-->
                </td>
                <td class="ranking-points">{{result.points - result.bonus}}</td>
                <td class="ranking-points">{{result.bonus}}</td>
                <td class="ranking-points-total">{{result.points}}</td>
              </tr>
            </table>
          </div>
          <!-- Rankings End -->
        </div>
      </div>
    </main>
    <div id="push"></div>
    <footer-component></footer-component>
  </div>
  `,
  data(){
    return {
      rank: [],
      user: {
        'username': "",
        'points': 0,
        'bonus': 0,
        'position': 0
      },
      headerKey: 0,
    }
  },
  mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
    })
    this.init();
  },
  watch:{
    user: function(val){
      /*   ************************************************** */
      console.log("cambio");
    }
  },
  methods: {
    resize() {
      this.headerKey++;
    },
    init() {
      this.chargeRank();
    },
    getImage(user){
      var index = this.rank.findIndex(u => u.nickname === user.nickname);
      if(index === 0)
        return "static/images/Badges/1stClassified.png";
      else if(index === 1)
        return "static/images/Badges/2ndClassified.png";
      else if(index === 2)
        return "static/images/Badges/3rdClassified.png";
      else
        return "";
    },
    getImageTop10(user){
      var index = this.rank.findIndex(u => u.nickname === user.nickname);
      if(index < 10)
        return "static/images/Badges/BadgeTop10.png";
      else
        return "";
    },
    getImageTop100(user){
      var index = this.rank.findIndex(u => u.nickname === user.nickname);
      if(index < 100)
        return "static/images/Badges/BadgeTop100.png";
      else
        return "";
    },
    getColor(user){
      var index = this.rank.findIndex(u => u.nickname === user.nickname);
      var color = {
        'even': "background-color: white",
        'odd': "background-color: #f0f7f6",
        'logged': "background-color: #7df779",
      };
      if (user._id == sessionStorage.getItem("user_session"))
        return color.logged;
      else if (index % 2 == 0)
        return color.even;
      else
        return color.odd;
    },
    getPosition(user){
      return this.rank.findIndex(u => u.nickname === user.nickname) + 1;
    },
    getUser() {
      var userId = sessionStorage.getItem("user_session");
      axios.get("/api/users/" + userId)
      .then(result => {
        this.user.username = result.data.nickname;
        this.user.points = result.data.points;
        this.user.bonus = result.data.bonus;
        this.user.position = this.rank.findIndex(u => u.nickname === result.data.nickname);
      });
    },
    goToProfile(result) {
      this.$router.push({ name:'others', params: {username: result.nickname}});
    },
    chargeRank() {
      axios.get("/api/userRanking")
      .then(result => {
        result.data.forEach(user => {
          this.rank.push(user);
        });
        this.getUser();
      });
    }
  }
}
