const Board = {
  template: `
  <div class="body">
    <headerbar :key="headerKey"></headerbar>
    <main>
      <div class="main">
        <shortbar></shortbar>
        <div class="main-body">
          <!-- Info Badges -->
          <div class="infoBadges-container">
            <div class="infoBadges-title-container">
              <div class="infoBadges-title">
                Achievements di <!--{{this.name}}-->
              </div>
              <div class="info-badges-user-info">
              <div class="info-badges-avatar-image-container" @click.prevent="goToOwnerProfile">
                <img class="info-badges-avatar-image" :src="owner.img">
              </div>  
              <div class="info-badges-user-nickname" @click.prevent="goToOwnerProfile">
                {{this.owner.nickname}}
              </div>
            </div>
            </div>
            <div class="legend-container">
              <div class="legend">
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/BadgeTop100.png" :style="gray('static/images/Badges/BadgeTop100.png')" />
                  </div>
                    <div class="info-badge-board">
                      <div class="badge-description">
                        <div class="badge-description-text">
                          Classificati fra i primi 100.
                        </div>
                        <div class="badge-description-pti">
                          (5 pti)
                        </div>
                      </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/BadgeTop100.png')" class="badgeBoard-grade-container">
                      <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{position}}°</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.posSit(100,1000),1000)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/BadgeTop10.png" :style="gray('static/images/Badges/BadgeTop10.png')"/>
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Classificati fra i primi 10.
                      </div>
                      <div class="badge-description-pti">
                        (10 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/BadgeTop10.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                      <div class="badgeBoard-number">{{position}}°</div>
                      <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.posSit(10,100),100)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/Badge_10_Positive.png" :style="gray('static/images/Badges/Badge_10_Positive.png')"/>
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Ricevi 10 valutazioni positive su una tua recensione.
                      </div>
                      <div class="badge-description-pti">
                        (10 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/Badge_10_Positive.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{this.ratesBest}}/10</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.ratesBest,10)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/Badge_50_Positive.png" :style="gray('static/images/Badges/Badge_50_Positive.png')"/>
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Ricevi 50 valutazioni positive su una tua recensione.
                      </div>
                      <div class="badge-description-pti">
                        (50 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/Badge_50_Positive.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{this.ratesBest}}/50</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.ratesBest,50)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/Badge_100_Positive.png" :style="gray('static/images/Badges/Badge_100_Positive.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Ricevi 100 valutazioni positive su una tua recensione.
                      </div>
                      <div class="badge-description-pti">
                        (100 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/Badge_100_Positive.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{this.ratesBest}}/100</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.ratesBest,100)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/Badge_200_Positive.png" :style="gray('static/images/Badges/Badge_200_Positive.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Ricevi 200 valutazioni positive su una tua recensione.
                      </div>
                      <div class="badge-description-pti">
                        (200 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/Badge_200_Positive.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{this.ratesBest}}/200</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.ratesBest,200)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/1stClassified.png" :style="gray('static/images/Badges/1stClassified.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Raggiungi la prima posizione in classifica.
                      </div>
                      <div class="badge-description-pti">
                        (100 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/1stClassified.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{position}}°</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.posSit(1, 100),100)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/2ndClassified.png" :style="gray('static/images/Badges/2ndClassified.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Raggiungi la seconda posizione in classifica.
                      </div>
                      <div class="badge-description-pti">
                        (75 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/2ndClassified.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{position}}°</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.posSit(2,100),100)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/3rdClassified.png" :style="gray('static/images/Badges/3rdClassified.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Raggiungi la terza posizione in classifica.
                      </div>
                      <div class="badge-description-pti">
                        (50 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/3rdClassified.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{position}}°</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.posSit(3,100),100)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/BadgeGrade1.png" :style="gray('static/images/Badges/BadgeGrade1.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Valuta 10 recensioni create da altri utenti.
                      </div>
                      <div class="badge-description-pti">
                        (10 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/BadgeGrade1.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{like}}/10</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.like,10)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
              </div>
              <div class="legend">
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/BadgeGrade2.png" :style="gray('static/images/Badges/BadgeGrade2.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Valuta 50 recensioni create da altri utenti.
                      </div>
                      <div class="badge-description-pti">
                        (50 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/BadgeGrade2.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{like}}/50</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.like,50)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/BadgeGrade3.png" :style="gray('static/images/Badges/BadgeGrade3.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Valuta 100 recensioni create da altri utenti.
                      </div>
                      <div class="badge-description-pti">
                        (100 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/BadgeGrade3.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{like}}/100</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.like,100)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/BadgeGrade4.png" :style="gray('static/images/Badges/BadgeGrade4.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Valuta 200 recensioni create da altri utenti.
                      </div>
                      <div class="badge-description-pti">
                        (200 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/BadgeGrade4.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{like}}/200</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.like,200)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/BadgeLevel1.png" :style="gray('static/images/Badges/BadgeLevel1.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Crea la tua prima recensione.
                      </div>
                      <div class="badge-description-pti">
                        (10 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/BadgeLevel1.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{review}}/1</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.review,1)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/BadgeLevel2.png" :style="gray('static/images/Badges/BadgeLevel2.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Crea 10 recensioni.
                      </div>
                      <div class="badge-description-pti">
                        (50 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/BadgeLevel2.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{review}}/10</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.review,10)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/BadgeLevel3.png" :style="gray('static/images/Badges/BadgeLevel3.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Crea 50 recensioni.
                      </div>
                      <div class="badge-description-pti">
                        (100 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/BadgeLevel3.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{review}}/50</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.review,50)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/BadgeLevel4.png" :style="gray('static/images/Badges/BadgeLevel4.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Crea 100 recensioni.
                      </div>
                      <div class="badge-description-pti">
                        (200 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/BadgeLevel4.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">{{review}}/100</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(this.review,100)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/LibriBadge.png" :style="gray('static/images/Badges/LibriBadge.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Crea la tua prima recensione su un libro.
                      </div>
                      <div class="badge-description-pti">
                        (5 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/LibriBadge.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">0/1</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(0,1)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/GiochiBadge.png" :style="gray('static/images/Badges/GiochiBadge.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Crea la tua prima recensione su un gioco.
                      </div>
                      <div class="badge-description-pti">
                        (5 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/GiochiBadge.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(5,5)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">0/1</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(0,1)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
                <div class="info-badge">
                  <div class="badge-img">
                    <img src="/static/images/Badges/Film_Serie_TVBadge.png" :style="gray('static/images/Badges/Film_Serie_TVBadge.png')" />
                  </div>
                  <div class="info-badge-board">
                    <div class="badge-description">
                      <div class="badge-description-text">
                        Crea la tua prima recensione su un Film/Serie TV.
                      </div>
                      <div class="badge-description-pti">
                        (5 pti)
                      </div>
                    </div>
                    <div class="badgeBoard">
                      <div v-if="isin('static/images/Badges/Film_Serie_TVBadge.png')" class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">&#x2714;</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(1,1)"></div>
                      </div>
                      <div v-else class="badgeBoard-grade-container">
                        <div class="badgeBoard-number">0/1</div>
                        <div class="quote four-half-stars achievement-progress" :style="this.setWidthVote(0,1)"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="badge-line"/>
              </div>
            </div>
          </div>
          <!-- Info Badges End -->
        </div>
      </div>
    </main>
    <div id="push" style="height: 50px"></div>
    <footer-component></footer-component>
  </div>
  `,
  data(){
    return {
      owner: "",
      badges: [],
      name: "",
      like: 0,
      review: 0,
      ratesBest: 0,
      position: 1000,
      maxPos: 1000,
      headerKey: 0,
    }
  },
  mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
    })
    this.badges= [];
    this.getOwnerInfo();
    this.getBadge();
  },
  methods:{
    resize() {
      this.headerKey++;
    },
    getOwnerInfo() {
      axios. get("/api/users/" + this.$route.params.user)
      .then(result => {
        this.owner = result.data;
      });
    },
    getBadge(){
      var userId = this.$route.params.user;
      axios.get("/api/users/" + userId)
      .then(result => {
        if(result){
          this.name = result.data.nickname;
          this.badges = this.clearDoubleBadge(result.data.badges);
          this.like = result.data.votes;
          axios.get("/api/reviews/user/" + result.data._id)
          .then(revRes => {
            this.review = revRes.data.length;
            this.ratesBest = this.bestPoints(revRes.data);
          });
          axios.get("/api/userRanking")
          .then(rankRes => {
            var pos = rankRes.data.findIndex(u => u.nickname === this.name) + 1;
            this.position = pos < this.maxPos? pos : this.maxPos;
            //console.log(this.position);
          });
        }
        else this.$router.push('/404');
      })
      .catch(() => this.$router.push('/404'));
    },
    bestPoints(reviews){
      var best = 0;
      reviews.forEach(review => {
        var filter = review.rates.filter((rate) => rate.grade >= 3);
        best = filter.length > best? filter.length : best;
      });
      return best;
    },
    clearDoubleBadge(badges){
      var badgeToSave = [];
      badges.forEach(badge => {
        if(!badgeToSave.includes(badge))
          badgeToSave.push(badge);
      });
      return badgeToSave;
    },
    gray(element){
      if(this.isin(element))
        return "filter: grayscale(0);";
      else return "filter: grayscale(1);";
    },
    isin(element){
      return this.badges.includes(element);
    },
    image(path){
      return "/" + path;
    },
    setWidthVote: function(quote, max) {
      if(quote > 0) {
        if(quote > max) {
          quote = max;
        }
        var currentQuote = "width: " + JSON.parse(quote * 100 / max) + "%";
        if(max == 200)
          console.log(currentQuote);
        return currentQuote;
      } else {
        return "width: 0%";
      }
    },
    posSit(pos, max){
      var diff = this.position - pos;
      if (diff >= max)
        return 0;
      else return diff <= 0? max : max - diff;
    },
    goToOwnerProfile() {
      if(this.owner._id == sessionStorage.getItem("user_session"))
        this.$router.push("/profile");
      else
        this.$router.push({ name:'others', params: {username: this.owner.nickname}})
    }
  }
}
