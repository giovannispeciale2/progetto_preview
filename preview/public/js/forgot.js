const Forgot = {
  template: `
  <div class="body">
    <headerbar :key="headerKey"></headerbar>
    <main>
      <div class="main">
        <!-- Forgot -->
        <div class="log">
          <div class="login-box" :style="forgotBoxHeight">
            <div class="login-imgLocked-container">
              <img class="avatar" src="static/images/previewNewLogin.png" alt="Avatar" >
            </div>
            <div class="forgot-email-container">
              <input class="mail" v-model="userData.email" type="text" id="mail" placeholder="Inserisci email di recupero" required>
            </div>
            <button class="forgotButton" @click.prevent = "forgot" type="submit" :disabled="disabled()">Invia e-mail</button>

            <hr class="login-box-line">

            <div class="login-signin-box">
              <div>Ritorna al login! <a class="login-forgot-link" @click.prevent = "login">Accedi</a></div>
            </div>
          </div>
        </div>
        <!-- Forgot End -->
        <!-- Alert modal -->
          <div class="modal fade" id="forgotModal" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Recupero password</h4>
                </div>
                <div class="modal-body alert-body">
                  <p>{{alertText}}</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="profile-closeButtonMenù" data-dismiss="modal" @click.prevent="closeModal">Chiudi</button>
                </div>
              </div>
              
            </div>
          </div>
          <!-- Alert modal end -->
      </div>
    </main>
    <div id="push"></div>
    <footer-component></footer-component>
  </div>
  `,
  data() {
    return {
      userData: {
        email: ""
      },
      forgotBoxHeight: "height: 40%;",
      headerKey: 0,
      alertText: "",
      wrongMail: false,
    }
  },
  mounted() {
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
    })
  },
  methods: {
    resize() {
      this.headerKey++;
    },
    forgot() {
      axios.get("/api/forgot", {
        params: {
          mail: this.userData.email
        }
      })
      .then(response => {
        if(response.data.error) {
          this.alertText = response.data.error;
          this.wrongMail = true;
          $('#forgotModal').modal('show');
        } else {
          this.alertText = response.data.success;
          $('#forgotModal').modal('show');
        }
      });
    },
    closeModal() {
      if(this.wrongMail) 
        this.wrongMail = false;
      else  
        this.$router.push("/");
    },
    disabled() {
      if(this.userData.email == "")
        return true;
      else
        return false;
    },
    login() {
      this.$router.push('login');
    }
  }
}
