const Home = {
  template: `
  <div class="body">
    <headerbar :key="headerKey"></headerbar>
    <main>
      <div class="main">
        <shortbar></shortbar>
        <div class="main-body">
          <div class="cards-container">
            <div class="card-section-container">
              <div class="card-section-title-container">
                <div class="card-section-title">
                  Più positive
                </div>
                <div class="card-section-button-container">
                  <button class="card-section-button" @click.prevent="showMoreBestRated()" type="submit">Mostra Altro</button>
                </div>
              </div>

              <div v-for="review in bestRatedReviews" class="card" :style="setBorderColor(review.color)" ref="card" id="card">
                <div class="card-container">
                  <div class="card-image-container">
                    <router-link :to="{ name:'review', params: {review: review.id, work: review.workId}}">
                      <img class="card-image" v-bind:src="review.workImg"/>
                    </router-link>
                  </div>
                  <div class="card-review-title">
                    <router-link :to="{ name:'review', params: {review: review.id, work: review.workId}}">
                      <a :title="review.reviewTitle">
                        {{review.reviewTitle | limit(15)}}
                      </a>
                    </router-link>
                  </div>
                  <div class="card-work-title" :title="review.workTitle">
                    <!--<router-link :to="{ name:'work', params: {work: review.workId}}">
                      <a :title="review.workTitle">-->
                        {{review.workTitle | limit(20)}}
                      <!--</a>
                    </router-link>-->
                  </div>
                  <div class="card-user">
                    <router-link :to="{ name:'others', params: {username: review.userNickname}}" class="link">
                      <a :title="review.userNickname">
                        <div class="card-user-container">
                          <div class="card-user-avatar">
                            <img :src="review.userImg">
                          </div>
                          <div class="card-user-nickname" :title="review.userNickname">
                            {{review.userNickname | limit(16)}}
                          </div>
                        </div>
                      </a>
                    </router-link>
                  </div>
                  <div class="card-rating">
                    <div class="rating-value">
                      {{review.reviewAvgRate}}
                    </div>
                    <div class="ratings">
                      <div class="empty-stars"></div>
                      <div class="full-stars" :style="renderRating(review.reviewAvgRate)"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="card-section-container">
              <div class="card-section-title-container">
                <div class="card-section-title">
                  Più recenti
                </div>
                <div class="card-section-button-container">
                  <button class="card-section-button" @click.prevent="showMoreMostRecent()" type="submit">Mostra Altro</button>
                </div>
              </div>

              <div v-for="review in mostRecentReviews" class="card" :style="setBorderColor(review.color)">
                <div class="card-container">
                  <div class="card-image-container">
                    <router-link :to="{ name:'review', params: {review: review.id, work: review.workId}}">
                      <img class="card-image" v-bind:src="review.workImg"/>
                    </router-link>
                  </div>
                  <div class="card-review-title">
                    <router-link :to="{ name:'review', params: {review: review.id, work: review.workId}}">
                      <a :title="review.reviewTitle">
                        {{review.reviewTitle | limit(15)}}
                      </a>
                    </router-link>
                  </div>
                  <div class="card-work-title" :title="review.workTitle">
                    <!--<router-link :to="{ name:'work', params: {work: review.workId}}">
                      <a :title="review.workTitle">-->
                        {{review.workTitle | limit(20)}}
                      <!--</a>
                    </router-link>-->
                  </div>
                  <div class="card-user">
                    <router-link :to="{ name:'others', params: {username: review.userNickname}}" class="link">
                      <a :title="review.userNickname">
                        <div class="card-user-container">
                          <div class="card-user-avatar">
                            <img :src="review.userImg">
                          </div>
                          <div class="card-user-nickname" :title="review.userNickname">
                            {{review.userNickname | limit(16)}}
                          </div>
                        </div>
                      </a>
                    </router-link>
                  </div>
                  <div class="card-rating">
                    <div class="rating-value">
                      {{review.reviewAvgRate}}
                    </div>
                    <div class="ratings">
                      <div class="empty-stars"></div>
                      <div class="full-stars" :style="renderRating(review.reviewAvgRate)"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <div id="push" :style="pushHeight"></div>
    <footer-component></footer-component>
  </div>
  `,
  data(){
    return {
      sliceSize: 5,
      sliceSizeBestRatedMultiplier: 1,
      sliceSizeMostRecentMultiplier: 1,
      categories: [],
      mostRecentReviews: [],
      bestRatedReviews: [],
      pushHeight: '',
      headerKey: 0,
    }
  },
  mounted() {
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
      this.init();
    })
  },
  methods: {
    mediaQuery() {
      var w = window.innerWidth;
      if (w >= 1400) {
        this.sliceSize = 5;
      } else if (w < 1400 && w >= 650) {
        this.sliceSize = 4;
      } else if (w < 650 && w >= 500) {
        this.sliceSize = 3;
      } else if (w < 500) {
        this.sliceSize = 2;
      }
    },
    resize() {
      setTimeout(() => {
        this.headerKey++;
        this.mediaQuery();
        this.flushLists();
        this.getBestRatedReviews();
        this.getMostRecentReviews();
        var pushSize = this.getPushSize();
        this.pushHeight = "height: "
            + parseFloat((pushSize) + (pushSize * (this.sliceSizeBestRatedMultiplier - 1)) + (pushSize * (this.sliceSizeMostRecentMultiplier - 1)))
            + "px";
      }, 100);
    },
    flushLists: function() {
      this.mostRecentReviews = [];
      this.bestRatedReviews = [];
    },
    init: function() {
      // this.mediaQuery();
      this.listCategories();
      // this.getBestRatedReviews();
      // this.getMostRecentReviews();
      this.resize();
    },
    listCategories: function() {
      axios.get("/api/categories")
      .then(response => this.categories = response.data)
      .catch(err => console.log(err));
    },
    getBestRatedReviews: function() {
      this.bestRatedReviews = [];
      axios.get("/api/reviews/best_rated_reviews")
      .then(response => {
        var bestRated = response.data.slice(0, (this.sliceSizeBestRatedMultiplier * this.sliceSize));
        bestRated.forEach(review => {
          this.renderReviews(review, this.bestRatedReviews);
        });
      })
      .catch(err => console.log(err));
    },
    getMostRecentReviews: function() {
      this.mostRecentReviews = [];
      axios.get("/api/reviews/most_recent_reviews")
      .then(response => {
        var mostRecent = response.data.slice(0, (this.sliceSizeMostRecentMultiplier * this.sliceSize));
        mostRecent.forEach(review => {
          this.renderReviews(review, this.mostRecentReviews);
        });
      })
      .catch(err => console.log(err));
    },
    renderReviews: function(review, listWhereSaving) {
      axios.get('/api/works/' + review.work)
      .then(responseWork => {
        axios.get('/api/users/' + review.user)
        .then(responseUser => {
          var renderedResult = {};
          renderedResult.id = review._id;
          renderedResult.reviewTitle = review.title;
          renderedResult.reviewText = review.text;
          renderedResult.reviewDate = review.date;
          renderedResult.reviewAvgRate = review.avg.toFixed(2);
          renderedResult.workId = review.work;
          renderedResult.workTitle = responseWork.data.title;
          renderedResult.workImg = responseWork.data.img;
          renderedResult.userId = review.user;
          renderedResult.userImg = responseUser.data.img;
          renderedResult.category = responseWork.data.category;
          renderedResult.color = "";
          for(i = 0; i < this.categories.length; i++) {
            if(this.categories[i].name == renderedResult.category)
              renderedResult.color = this.categories[i].color;
          }
          renderedResult.userNickname = responseUser.data.nickname;
          listWhereSaving.push(renderedResult);
        })
        .catch(error => console.log(error));
      })
      .catch(error => console.log(error));
    },
    setBorderColor: function(color) {
      return "border: solid 3px " + color + ";";
    },
    renderRating: function(avg) {
      // avg : 5 = x : 100
      var perc = avg * 100 / 5;
      return "width: " + perc + "%";
    },
    showMoreMostRecent() {
      this.sliceSizeMostRecent += this.sliceSize;
      this.sliceSizeMostRecentMultiplier++;
      this.mostRecentReviews = [];
      this.getMostRecentReviews();
      this.pushFooter();
    },
    showMoreBestRated() {
      this.sliceSizeBestRated += this.sliceSize;
      this.sliceSizeBestRatedMultiplier++;
      this.bestRatedReviews = [];
      this.getBestRatedReviews();
      this.pushFooter();
    },
    pushFooter() {
      var oldPushHeight = this.pushHeight.substring(7, this.pushHeight.length - 2);
      var newPushHeight = "height: " + (parseFloat(oldPushHeight) + parseFloat(this.getPushSize())) + "px";
      this.pushHeight = newPushHeight;
    },
    getPushSize() {
      var w = window.innerWidht;
      w = 1400;
      if (w > 1400)
        return 365;
      else if (w > 1000)
        return 323;
      else if (w > 900)
        return 301;
      else if (w > 800)
        return 280;
      else if (w > 700)
        return 259;
      else if (w > 650)
        return 238;
      else if (w > 550)
        return 301;
      else
        return 280;
    },
  },
  filters: {
    limit: function(text, length) {
      if(text.length > length - 3)
        return text.substring(0, length - 3) + "...";
      else
        return text;
    }
  }

}
