const AddReview = {
  template: `
    <div class="body">
      <headerbar :key="headerKey"></headerbar>
      <main>
        <div class="main">
          <shortbar></shortbar>
          <div class="main-body">
            <!-- Add Review -->
            <div class="add">
              <div class="addingReview">
                <form class="form-adding-container" autocomplete="off">
                  <h2>Aggiungi una nuova recensione</h2>
                  <div class="container-add-insertBar" v-show="showSearchWorks">
                    <input class="insertBar" v-model="workToSearch" type="text" id="search" placeholder="Cerca l'opera da recensire"/>
                  </div>
                  <div class="works-suggested" v-show="showWorks">
                    <h4 id="titleSuggestedWorks">Opere Suggerite:</h4>
                    <div class="work-suggested" v-for="work in works" :key="work._id">
                      <label @click.prevent="selectWork(work._id)">{{work.title}}</label>
                    </div>
                    <div class="work-suggested" v-if="works.length == 0">
                      Nessun risultato trovato per {{workToSearch}}, riprova
                    </div>
                    <h4 id="orCreateNewWork">Oppure:</h4>
                    <input title="Crea Nuova Opera" value="Crea una nuova opera" type="submit" class="button-new-work" @click.prevent="createWork"/>
                  </div>
                  <div class="new-work-container" v-show="showNewWork">
                    <div class="new-work">
                      <h4>Nuova Opera</h4>
                      <hr id="new-review-line"/>
                      <div v-if="wrongWorkData" class="addReview-wrongData">
                        Compila tutti i campi correttamente.
                      </div>
                      <div v-if="workAlreadyExists" class="workAlreadyExists">
                        Opera già esistente.
                      </div>
                      <div class="new-work-content">
                        <div class="select-category">
                          <select v-model="newWork.category" class="select-work-category insertBarWork" required>
                            <option value="" disabled selected hidden>Categoria</option>
                            <option v-for="category in categories" :key="category.id" :value="category.name">{{category.name}}</option>
                          </select>
                        </div>
                        <div class="select-genre" v-if="newWork.category">
                          <select v-model="newWork.genre" class="select-work-category insertBarWork">
                            <option value="" style="color: gray;" disabled selected hidden>Genere</option>
                            <option v-for="genre in genres" :value="genre">{{genre}}</option>
                          </select>
                        </div>
                        <div class="insert-title-work">
                          <input v-model="newWork.title" type="text" placeholder="Titolo" title="Titolo" class="insertBarWork" required/>
                        </div>
                        <div class="insert-author-work">
                          <input class="insertBarWork" v-model="newWork.author" type="text" title="Autore" placeholder="Autore" required/>
                        </div>
                        <div class="insert-date-work">
                          <input class="datePicker insertBarWork" v-model="newWork.date" type="date" title="Data Pubblicazione" required/>
                        </div>
                        <div class="insert-img-work">
                          <cropperwork @uploaded:img="imgName"></cropperwork>
                        </div>
                        <div class="new-work-buttons-container">
                          <input title="Crea" value="Crea" type="submit" @click.prevent="submitWork" class="button-submit-work"/>
                          <input title="Cancella Tutto" value="Cancella Tutto" type="submit" @click.prevent="cancelCreateWork" class="button-reset"/>
                          <input class="button-cancel-add button-cancel-new-review" @click.prevent="cancelAddReview" title="Annulla" value="Annulla" type="submit"/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="new-review-container" v-show="showTextAreas" :style="this.setBorderColor(color)">
                    <h4>Nuova Recensione per "{{selectedWork.title}}"</h4>
                    <hr id="new-review-line"/>
                    <div v-if="wrongReviewData" class="addReview-wrongData">
                      Non puoi inserire una recensione senza il titolo o il testo.
                    </div>
                    <div class="new-review">
                      <div class="new-review-content">
                        <div class="img-work-container">
                          <img class="new-review-work-img" v-bind:src="selectedWork.img"/>
                        </div>
                        <div class="text-review-container">
                          <input v-model = "review.title" type="text" class="insertBar new-review-title-bar" id="title" placeholder="Titolo">
                          <textarea class="new-review-text-area" v-model="review.text" id="text" placeholder="Testo" rows="8" cols="5"></textarea>

                        </div>
                      </div>
                      <div class="new-review-buttons-container">
                        <input title="Crea Recensione" value="Crea Recensione" @click.prevent="submitReview" type="submit" class="button-submit-review"/>
                        <input class="button-cancel-add button-cancel-new-review" @click.prevent="cancelAddReview" title="Annulla" value="Annulla" type="submit"/>
                      </div>

                    </div>
                  </div>
                </form>
              </div>
              <div class="container-button-cancel-add" v-show="showCancelButton()">
                <input class="button-cancel-add" @click.prevent="cancelAddReview" title="Annulla" value="Annulla" type="submit"/>
              </div>
            </div>
            <!-- Add Review End -->
          </div>
        </div>
      </main>
      <div id="push"></div>
      <footer-component></footer-component>
    </div>
  `,
  data(){
    return {
      categories: [],
      workToSearch: "",
			works: "",
			showSearchWorks: true,
			showWorks: false,
			showNewWork: false,
			showTextAreas: false,
			selectedWork: "",
			review: {
				title: "",
				text: "",
        user: ""
			},
			genres: [],
			newWork: {
				title: "",
				author:"",
				date: "",
				img: "",
				category: "",
				genre: ""
			},
      color: "",
      wrongWorkData: false,
      wrongReviewData: false,
      workAlreadyExists: false,
      headerKey: 0,
    }
  },
  mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
    })
    this.init();
  },
  watch: {
    workToSearch: function(val, oldVal) {
      if(val != "")
        this.search();
      else
        this.showWorks = false;
    },
    'newWork.category': function(val, oldVal) {
      if((val != undefined) && (val != "") && (val != null)) {
        var i = this.categories.findIndex(item => item.name === val);
        this.genres = this.categories[i].genres;
      }
    },
  },
  methods: {
    resize() {
      this.headerKey++;
    },
    showCancelButton() {
      if(this.showTextAreas)
        return false;
      else if(this.showNewWork)
        return false;
      else
        return true;
    },
    imgName(string) {
      this.newWork.img = string;
    },
    init: function() {
      this.listCategories();
    },
    listCategories: function() {
      axios.get("/api/categories")
      .then(response => {
        this.categories = response.data;
      })
      .then(response => {
        for(var cat in this.categories) {
          cat.showGenres = false;
        }
      })
      .catch(err => console.log(err));
    },
    cancelAddReview() {
      this.showTextAreas = false;
      this.showNewWork = false;
      this.workToSearch = "";
      this.showSearchWorks = true;
      this.clearFormWork();
      this.clearFormReview();
      this.$router.push("/");
    },
    clearFormWork: function() {
      this.wrongWorkData = false;
      this.workAlreadyExists = false;
      this.showWorks = false;
      this.newWork.title = "";
      this.newWork.author = "";
      this.newWork.date = "";
      this.newWork.img = "";
      this.newWork.category = "";
      this.newWork.genre = "";
    },
    clearFormReview: function() {
      this.wrongReviewData = false;
      this.review.title = "";
      this.review.text = "";
    },
    search() {
      axios.get('/api/works', {params: {wordToSearch: this.workToSearch}})
      .then(response => {
        this.showWorks = true;
        this.works = response.data;
      })
      .catch(error => (console.log(error)));
    },
    selectWork(_id) {
      var i = this.works.findIndex(item => item._id === _id);
      this.selectedWork = this.works[i];
      for(i = 0; i < this.categories.length; i++) {
        if(this.categories[i].name == this.selectedWork.category)
          this.color = this.categories[i].color;
      }
      this.showSearchWorks = false;
      this.showWorks = false;
      this.showTextAreas = true;
    },
    createWork() {
      this.showSearchWorks = false;
      this.showWorks = false;
      this.showNewWork = true;
    },
    cancelCreateWork() {
      this.workToSearch = "";

      this.clearFormWork();
    },
    selectCategory(_id) {
      var i = this.categories.findIndex(item => item._id === _id);
      this.newWork.category = this.categories[i].name;
    },
    selectGenre(genre) {
      this.newWork.genre = genre;
    },
    submitWork() {
      if(this.newWork.title === "" || this.newWork.category === "" || this.newWork.genre === "" || this.newWork.author === "" || this.newWork.date === "")
        this.wrongWorkData = true;
      else {
        axios.post('/api/works', this.newWork)
        .then(response => {
          if(response.data.Error)
            this.workAlreadyExists = true;
          else {
            this.selectedWork = response.data;
            this.showNewWork = false;
            this.showTextAreas = true;
            this.clearFormWork();
            this.clearFormReview();
          }
        })
        .catch(error => (console.log(error)));
      }
    },
    submitReview() {
      this.review.work = this.selectedWork._id;
      this.review.user = sessionStorage.getItem("user_session");
      this.review.date = new Date();
      if(this.review.title === "" || this.review.text === "")
        this.wrongReviewData = true;
      else {
        axios.post('/api/reviews', this.review)
        .then(response => {
          this.clearFormWork();
          this.clearFormReview();
          this.$router.push({
              name: 'review',
              params: { review: response.data._id, work: response.data.work }
          });
        })
        .catch(error => (console.log(error)));
      }
    },
    setBorderColor(color) {
      return "border: solid 3px " + color + ";";
    },
  }
}
