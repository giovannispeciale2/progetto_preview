const Research = {
  template: `
  <div class="body">
    <headerbar :key="headerKey"></headerbar>
    <main>
      <div class="main">
        <shortbar></shortbar>
        <div class="main-body">
          <!-- Search Review -->
          <div class="cards-container">
            <div class="card-section-container">
              <div class="card-section-title-container">
                <div class="card-section-title" v-if="searchResults.length > 0">
                  Risultati per: "{{title}}"
                </div>
                <div class="card-section-title" v-if="searchResults.length == 0">
                  Nessun risultato trovato per "{{title}}".
                </div>
              </div>
              <div v-for="result in searchResults" class="card" :style="setBorderColor(result.color)">
                <div class="card-container">
                  <div class="card-image-container">
                    <router-link :to="{ name:'review', params: {review: result.id, work: result.workId}}">
                      <img class="card-image" v-bind:src="result.workImg"/>
                    </router-link>
                  </div>
                  <div class="card-review-title">
                    <router-link :to="{ name:'review', params: {review: result.id, work: result.workId}}">
                      <a :title="result.reviewTitle">
                        {{result.reviewTitle | limit(15)}}
                      </a>
                    </router-link>
                  </div>
                  <div class="card-work-title" :title="result.workTitle">
                    <!--<router-link :to="{ name:'work', params: {work: result.workId}}">
                      <a :title="result.workTitle">-->
                        {{result.workTitle | limit(20)}}
                      <!--</a>
                    </router-link>-->
                  </div>
                  <div class="card-user">
                  <router-link :to="{ name:'others', params: {username: result.userNickname}}">
                      <a :title="result.userNickname">
                        <div class="card-user-container">
                          <div class="card-user-avatar">
                            <img :src="result.userImg">
                          </div>
                          <div class="card-user-nickname" :title="result.userNickname">
                            {{result.userNickname | limit(16)}}
                          </div>
                        </div>
                      </a>
                    </router-link>
                  </div>
                  <div class="card-rating">
                    <div class="rating-value">
                      {{result.reviewAvgRate}}
                    </div>
                    <div class="ratings">
                      <div class="empty-stars"></div>
                      <div class="full-stars" :style="renderRating(result.reviewAvgRate)"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Search Review End -->
        </div>
      </div>
    </main>
  </div>
  `,
  data(){
    return {
      categories: [],
      title: "",
      searchResults: [],
      color: "",
      headerKey: 0,
    }
  },
  watch: {
    '$route.params.title': function(val){
      this.title = val;
      this.searchResults = [];
      this.getReviews();
    }
  },
  mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
    })
    this.init();
    this.getReviews();
  },
  methods: {
    init: function(){
      this.title = this.$route.params.title;
      this.listCategories();
    },
    resize() {
      this.headerKey++;
    },
    listCategories: function() {
      axios.get("/api/categories")
      .then(response => this.categories = response.data)
      .catch(err => console.log(err));
    },
    getReviews: function(){
        axios.get('/api/works', {params: {wordToSearch: this.title}})
				.then(response => {
					response.data.forEach(work => {
						axios.get('/api/reviews', {params: {_id: work._id}})
						.then(response => {
							response.data.forEach(review => {
								axios.get('/api/works/' + review.work)
								.then(responseWork => {
									axios.get('/api/users/' + review.user)
									.then(responseUser => {
										var renderedResult = {};
                    renderedResult.id = review._id;
										renderedResult.reviewTitle = review.title;
										renderedResult.reviewText = review.text;
										renderedResult.reviewDate = review.date;
                    renderedResult.reviewAvgRate = review.avg.toFixed(2);
										renderedResult.workId = review.work;
                    renderedResult.workTitle = responseWork.data.title;

										renderedResult.workImg = responseWork.data.img;
										renderedResult.userId = review.user;
                    renderedResult.userImg = responseUser.data.img;
										renderedResult.category = responseWork.data.category;
										renderedResult.userNickname = responseUser.data.nickname;
                    for(i = 0; i < this.categories.length; i++) {
                      if(this.categories[i].name == renderedResult.category)
                        renderedResult.color = this.categories[i].color;
                    }
										this.searchResults.push(renderedResult);
									})
									.catch(error => console.log(error));
								})
								.catch(error => console.log(error));
							});
						})
						.catch(error => console.log(error));
					});
				})
				.then(response => {
					this.searchedReviews = true;
					// TODO
				})
				.catch(error => (console.log(error)));
    },
    renderRating: function(avg) {
      var perc = avg * 100 / 5;
      return "width: " + perc + "%";
    },
    setBorderColor: function(color) {
      return "border: solid 3px " + color + ";";
    },
  },
  filters: {
    limit: function(text, length) {
      if(text.length > length - 3)
        return text.substring(0, length-3)+"...";
      else
        return text;
    }
  }
}
