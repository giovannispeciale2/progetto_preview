const Review = {
  template: `
  <div class="body">
    <headerbar :key="headerKey"></headerbar>
    <main>
      <div class="main">
        <shortbar></shortbar>
        <div class="main-body">
          <div class="review-container">
            <!-- Show Review -->
            <div class="review-view-container" :style="this.setReviewStyle(reviewData.color)">
              <div class="review-view-title-container">
                <h2>{{reviewData.workTitle}}</h2>
                <div class="container-review-buttonRemove" v-if="isAdmin || isOwner">
                  <input title="Cancella Recensione" value="Cancella Recensione" class="review-buttonRemove" @click.prevent="deleteReview()" type="submit"/>
                </div>
              </div>
              <div>
                <hr id="review-view-line"/>
              </div>
              <div class="review-view">
                <div class="review-img-work-container">
                  <img :src="reviewData.workImg">
                </div>
                <div class="text-review-view-container">
                  <label class="review-title">{{reviewData.reviewTitle}}</label>
                  <router-link :to="{ name:'others', params: {username: reviewData.userNickname}}" class="link">
                    <div class="review-view-userData">
                      <img :src="reviewData.userImg">
                      <label class="review-userNickname">{{reviewData.userNickname}}</label>
                    </div>
                  </router-link>
                  <label class="review-reviewText">{{reviewData.reviewText}}</label>
                </div>

                <div class="review-evaluation-container">
                  <div class="review-evaluation-title">
                    <h2>Valutazione Attuale</h2>
                  </div>
                  <div class="review-evaluation-area">
                    <!-- Grade Review -->
                    <div class="review-global-grade">
                      <div class="review-actual-grade" title="Valutazione Media">{{reviewData.reviewAvg}}</div>
                      <div class="review-grades-container">
                        <div class="quote-container">
                          <div class="quote-number">5</div>
                          <div class="quote-grade-container" :title="this.setTitleVote(this.quote.five)">
                            <div class="quote five-stars" :style="this.setWidthVote(this.quote.five)"></div>
                          </div>
                        </div>

                        <div class="quote-container">
                          <div class="quote-number">4,5</div>
                          <div class="quote-grade-container" :title="this.setTitleVote(this.quote.halfFour)">
                            <div class="quote four-half-stars" :style="this.setWidthVote(this.quote.halfFour)"></div>
                          </div>
                        </div>

                        <div class="quote-container">
                          <div class="quote-number">4</div>
                          <div class="quote-grade-container" :title="this.setTitleVote(this.quote.four)">
                            <div class="quote four-stars" :style="this.setWidthVote(this.quote.four)"></div>
                          </div>
                        </div>

                        <div class="quote-container">
                          <div class="quote-number">3,5</div>
                          <div class="quote-grade-container" :title="this.setTitleVote(this.quote.halfThree)">
                            <div class="quote three-half-stars" :style="this.setWidthVote(this.quote.halfThree)"></div>
                          </div>
                        </div>

                        <div class="quote-container">
                          <div class="quote-number">3</div>
                          <div class="quote-grade-container" :title="this.setTitleVote(this.quote.three)">
                            <div class="quote three-stars" :style="this.setWidthVote(this.quote.three)"></div>
                          </div>
                        </div>

                        <div class="quote-container">
                          <div class="quote-number">2,5</div>
                          <div class="quote-grade-container" :title="this.setTitleVote(this.quote.halfTwo)">
                            <div class="quote two-half-stars" :style="this.setWidthVote(this.quote.halfTwo)"></div>
                          </div>
                        </div>

                        <div class="quote-container">
                          <div class="quote-number">2</div>
                          <div class="quote-grade-container" :title="this.setTitleVote(this.quote.two)">
                            <div class="quote two-stars" :style="this.setWidthVote(this.quote.two)"></div>
                          </div>
                        </div>

                        <div class="quote-container">
                          <div class="quote-number">1,5</div>
                          <div class="quote-grade-container" :title="this.setTitleVote(this.quote.halfOne)">
                            <div class="quote one-half-star" :style="this.setWidthVote(this.quote.halfOne)"></div>
                          </div>
                        </div>

                        <div class="quote-container">
                          <div class="quote-number">1</div>
                          <div class="quote-grade-container" :title="this.setTitleVote(this.quote.one)">
                            <div class="quote one-star" :style="this.setWidthVote(this.quote.one)"></div>
                          </div>
                        </div>

                        <div class="quote-container">
                          <div class="quote-number">0,5</div>
                          <div class="quote-grade-container" :title="this.setTitleVote(this.quote.half)">
                            <div class="quote half-star" :style="this.setWidthVote(this.quote.half)"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- Grade Review End -->
                  </div>
                </div>
              </div>
              <div class="valutation-container" v-if="!isOwner">
                <div class="valutation-evaluate-review">
                  <h4>Valuta Recensione</h4>
                </div>
                <div class="valutation-evaluate-review-stars">
                  <fieldset class="rating" >
                      <input v-model="grade" type="radio" id="star5" name="rating" value="5" v-on:click="saveRate(5)"/><label class = "full" for="star5" title="5 Stelle"></label>
                      <input v-model="grade" type="radio" id="star4half" name="rating" value="4.5" v-on:click="saveRate(4.5)"/><label class="half" for="star4half" title="4.5 Stelle"></label>
                      <input v-model="grade" type="radio" id="star4" name="rating" value="4" v-on:click="saveRate(4)"/><label class = "full" for="star4" title="4 Stelle"></label>
                      <input v-model="grade" type="radio" id="star3half" name="rating" value="3.5" v-on:click="saveRate(3.5)"/><label class="half" for="star3half" title="3.5 Stelle"></label>
                      <input v-model="grade" type="radio" id="star3" name="rating" value="3" v-on:click="saveRate(3)"/><label class = "full" for="star3" title="3 Stelle"></label>
                      <input v-model="grade" type="radio" id="star2half" name="rating" value="2.5" v-on:click="saveRate(2.5)"/><label class="half" for="star2half" title="2.5 Stelle"></label>
                      <input v-model="grade" type="radio" id="star2" name="rating" value="2" v-on:click="saveRate(2)"/><label class = "full" for="star2" title="2 Stelle"></label>
                      <input v-model="grade" type="radio" id="star1half" name="rating" value="1.5" v-on:click="saveRate(1.5)"/><label class="half" for="star1half" title="1.5 Stelle"></label>
                      <input v-model="grade" type="radio" id="star1" name="rating" value="1" v-on:click="saveRate(1)"/><label class = "full" for="star1" title="1 Stella"></label>
                      <input v-model="grade" type="radio" id="starhalf" name="rating" value="0.5" v-on:click="saveRate(0.5)"/><label class="half" for="starhalf" title="0.5 Stella"></label>
                  </fieldset>
                </div>
              </div>
            </div>

            <!-- Show Review End -->

            <!-- Show Reviews Work -->
            <div class="card-section-container" v-if="this.reviewsWork.length > 0">
              <div class="card-section-title review-card-section-title">
                Recensioni Sulla Stessa Opera
              </div>
              <div v-for="result in this.reviewsWork" class="card" :style="setBorderColor(result.color)">
                <div class="card-container">
                  <div class="card-image-container">
                    <router-link :to="{ name:'review', params: {review: result.reviewId, work: result.workId}}">
                      <img class="card-image" v-bind:src="result.workImg"/>
                    </router-link>
                  </div>
                  <div class="card-review-title">
                    <router-link :to="{ name:'review', params: {review: result.reviewId, work: result.workId}}">
                      <a :title="result.reviewTitle">
                        {{result.reviewTitle | limit(15)}}
                      </a>
                    </router-link>
                  </div>
                  <div class="card-work-title" :title="result.workTitle">
                  <!--<router-link :to="{ name:'work', params: {work: result.workId}}">
                    <a :title="result.workTitle">-->
                      {{result.workTitle | limit(20)}}
                    <!--</a>
                  </router-link>-->
                  </div>
                  <div class="card-user">
                    <router-link :to="{ name:'others', params: {username: result.userNickname}}" class="link">
                      <a :title="result.userNickname">
                        <div class="card-user-container">
                          <div class="card-user-avatar">
                            <img :src="result.userImg">
                          </div>
                          <div class="card-user-nickname" :title="result.userNickname">
                            {{result.userNickname | limit(16)}}
                          </div>
                        </div>
                      </a>
                    </router-link>
                  </div>
                  <div class="card-rating">
                    <div class="rating-value">
                      {{result.reviewAvgRate}}
                    </div>
                    <div class="ratings">
                      <div class="empty-stars"></div>
                      <div class="full-stars" :style="renderRating(result.reviewAvgRate)"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Show Reviews Work End -->

            <!-- Delete modal -->
            <div class="modal fade" id="deletedReviewModal" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Cancella recensione</h4>
                  </div>
                  <div class="modal-body alert-body" style="color: #696969">
                    <p>La recensione "{{reviewData.reviewTitle}}" è stata cancellata con successo.</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="profile-closeButtonMenù" data-dismiss="modal" @click.prevent="closeModal">Chiudi</button>
                  </div>
                </div>
                
              </div>
            </div>
            <!-- Delete modal end -->
            <!-- Alert modal -->
            <div class="modal fade" id="reviewModal" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Accedi</h4>
                  </div>
                  <div class="modal-body alert-body">
                    <p>Accedi per poter pubblicare e valutare recensioni.</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="profile-loginButtonMenù" data-dismiss="modal" @click.prevent="goToLogin">Accedi</button>
                    <button type="button" class="profile-closeButtonMenù" data-dismiss="modal" @click.prevent="closeNotLoggedModal">Chiudi</button>
                  </div>
                </div>
                
              </div>
            </div>
            <!-- Alert modal end -->

          </div>
        </div>
      </div>
    </main>
    <div id="push"></div>
    <footer-component></footer-component>
  </div>
  `,
  data(){
    return {
      categories: [],
      review: "",
      logged: false,
      user: "",
      isAdmin: false,
      reviewData: {},
      color: "",
      grade: 0,
      work: "",
      workData: {},
      reviewsWork: [],
      quote: {
        sum: 0,
        five: 0,
        halfFour: 0,
        four: 0,
        halfThree: 0,
        three: 0,
        halfTwo: 0,
        two: 0,
        halfOne: 0,
        one: 0,
        half: 0,
      },
      headerKey: 0,
      isOwner: false,
    }
  },
  watch: {
    '$route.params.review': function(val){
      this.reviewsWork = [];
      this.reviewData = {};
      this.workData = {};
      this.quote = {};
      this.grade = 0;
      this.init();
    }
  },
  mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
    })
    this.init();
  },
  methods: {
    resize() {
      this.headerKey++;
      if (window.innerWidth <= 650)
        this.$router.go();
    },
    init: function(){
      this.review = this.$route.params.review;
      // this.work = this.$route.params.work;
      this.listCategories();
      this.getUserLogged();
      this.getReview();
    },
    listCategories: function() {
      axios.get("/api/categories")
      .then(response => this.categories = response.data)
      .catch(err => console.log(err));
    },
    canRate: function(){
      this.user = sessionStorage.getItem("user_session");
      var reviewer = this.reviewData.userId;
      if(this.user && this.user !== reviewer)
        this.logged = true;
    },
    getUserLogged: function() {
      var userId = sessionStorage.getItem("user_session");
      axios.get("/api/users/" + userId)
      .then(result => {
        this.isAdmin = result.data.admin;
      });
    },
    getReview: function(){
      axios.get('/api/reviews', {params: {idR: this.review}})
      .then(responseReview =>{
        if(responseReview.data.length){
          var review = responseReview.data[0];
          axios.get('/api/works/' + review.work)
          .then(responseWork => {
            axios.get('/api/users/' + review.user)
            .then(responseUser => {
              var renderedResult = {};
              renderedResult.id = review._id;
              renderedResult.reviewTitle = review.title;
              renderedResult.reviewText = review.text;
              renderedResult.reviewDate = review.date;
              renderedResult.workId = review.work;
              this.work = review.work;
              renderedResult.workTitle = responseWork.data.title;
              renderedResult.workImg = responseWork.data.img;
              renderedResult.userId = review.user;
              renderedResult.reviewAvg = review.avg.toFixed(2);
              renderedResult.category = responseWork.data.category;
              renderedResult.userNickname = responseUser.data.nickname;
              renderedResult.userImg = responseUser.data.img;
              renderedResult.points = review.points;
              renderedResult.rates = review.rates;
              for(i = 0; i < this.categories.length; i++) {
                if(this.categories[i].name == responseWork.data.category)
                  renderedResult.color = this.categories[i].color;
              }
              this.reviewData = renderedResult;
              this.getVotesGrade(review.rates);
              this.canRate();
              if(this.logged)
                this.pastRate();
              if(review.user == sessionStorage.getItem("user_session"))
                this.isOwner = true;
              axios.get('/api/categories/')
              .then(response =>{
                response.data.forEach(category => {
                  if(category.name === renderedResult.category)
                     this.color = response.data.opacity;
                });
              });
              this.getWork();
            });
          });
        }
        else this.$router.push('/404');
      });
    },
    getWork: function() {
      axios.get('/api/works/' + this.work)
      .then(responseWork => {
        var renderedResult = {};
        renderedResult.workId = responseWork.data._id;
        renderedResult.workTitle = responseWork.data.title;
        renderedResult.workImg = responseWork.data.img;
        renderedResult.workCategory = responseWork.data.category;
        this.workData = renderedResult;
      });
      axios.get('/api/reviews/work', {params: {idW: this.work}})
      .then(response => {
        response.data.forEach(reviewWork => {
          axios.get('/api/users/' + reviewWork.user)
          .then(responseUser => {
            if(reviewWork._id !== this.review) {
              var dataResult = {};
              dataResult.reviewId = reviewWork._id;
              dataResult.workId = this.workData.workId;
              dataResult.reviewTitle = reviewWork.title;
              dataResult.points = reviewWork.points;
              dataResult.rates = reviewWork.rates;
              dataResult.reviewAvgRate = reviewWork.avg.toFixed(2);
              dataResult.userNickname = responseUser.data.nickname;
              dataResult.workImg = this.workData.workImg;
              dataResult.workTitle = this.workData.workTitle;
              dataResult.userImg = responseUser.data.img;
              dataResult.color = "";
              for(i = 0; i < this.categories.length; i++) {
                if(this.categories[i].name == this.workData.workCategory)
                  dataResult.color = this.categories[i].color;
              }
              this.reviewsWork.push(dataResult);
            }
          });
        });
      });
    },
    getVotesGrade: function(arrayRates) {
      arrayRates.forEach(quote => {
        if(quote.grade === "5")
          this.quote.five++;
        else if(quote.grade === "4.5")
          this.quote.halfFour++;
        else if(quote.grade === "4")
          this.quote.four++;
        else if(quote.grade === "3.5")
          this.quote.halfThree++;
        else if(quote.grade === "3")
          this.quote.three++;
        else if(quote.grade === "2.5")
          this.quote.halfTwo++;
        else if(quote.grade === "2")
          this.quote.two++;
        else if(quote.grade === "1.5")
          this.quote.halfOne++;
        else if(quote.grade === "1")
          this.quote.one++;
        else if(quote.grade === "0.5")
          this.quote.half++;
      });
      this.quote.sum = (this.quote.half + this.quote.one + this.quote.halfOne + this.quote.two + this.quote.halfTwo +
                    this.quote.three + this.quote.halfThree + this.quote.four + this.quote.halfFour + this.quote.five);
    },
    setWidthVote: function(quote) {
      if(quote > 0) {
        var currentQuote = "width: " + JSON.parse(quote * 100 / this.quote.sum) + "%";
        return currentQuote;
      } else {
        return "width: 0%";
      }
    },
    setTitleVote: function(quote) {
      if(quote > 0) {
        var roundedPercentage = JSON.parse(quote * 100 / this.quote.sum).toFixed(2);
        return "Voti " + quote + " = " + roundedPercentage + "%";
      }
      else
        return null;
    },
    pastRate: function(){
      var index = this.reviewData.rates.findIndex(rate =>rate.voter == this.user);
      if(this.reviewData.rates[index] != undefined) {
        this.grade = this.reviewData.rates[index].grade;
      }
    },
    getColor: function(){
      return this.color;
    },
    saveRate: function(grade){
      if(!this.logged) {
        $('#reviewModal').modal('show');
      } else {
        this.grade = grade;
        var review = this.reviewData.id;
        var reviewer = this.reviewData.userNickname;
        axios.get('/api/reviews/rate', {params: { idR: review, idU: this.user, grade: this.grade}})
        .then(response => {
          if(response.data.dif === -10){
            axios.get('/api/user/updatePoints', {params: {name: reviewer, grade: this.grade}});
            axios.get('/api/user/updateRates', {params: {id: this.user}});
          }
          else
            axios.get('/api/user/updatePoints', {params: {name: reviewer, grade: response.data.dif}});
          this.$router.go();
        });
        //-10 voto non esistente altrimenti diffenrenza
      }
    },
    goToLogin() {
      if(this.$route.fullPath != "/login")
        this.$router.push("/login");
    },
    deleteReview: function() {
      var reviewRemoved = {
        idR: this.review
      };
      axios.post('/api/reviews/deleteReview', reviewRemoved)
      .then(response => {
        $('#deletedReviewModal').modal('show');
      });
    },
    closeModal() {
      this.$router.go(-1);
    },
    closeNotLoggedModal() {
      this.$router.go();
    },
    setReviewStyle(color) {
      var val = this.setBorderColor(color);
      var w = window.innerWidth;
      if (w > 650)
        if(!this.logged)
          val += " height: 50%;";
        return val;
    },
    setBorderColor: function(color) {
      return "border: solid 3px " + color + ";";
    },
    renderRating: function(avg) {
      var perc = avg * 100 / 5;
      return "width: " + perc + "%";
    },
  },
  filters: {
    limit: function(text, length) {
      if(text.length > length - 3)
        return text.substring(0, length - 3) + "...";
      else
        return text;
    }
  }
}
