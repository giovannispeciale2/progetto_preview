const router = new VueRouter({
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes: [
    {path: '/genres/:categoria/:genere', name:'genres', component:Genres},
    {path: '/categories/:categoria', name:'categories', component:Categories},
    {path: '/rankings', name: 'rankings', component: Rank},
    {path: '/', component: Home},
    {path: '/login', component: Login},
    {path: '/signin', component: Signin},
    {path: '/research/:title', name:'research', component:Research},
    {path: '/review/:review', name:'review', component:Review, props: {review: true, work: false}},
    //{path: '/work/:work', name:'work', component:Work},
    {path: '/badgeBoard/:user', name:"badgeBoard", component:Board},
    {path: '/logout', component:Logout},
    {path: '/addReview', component:AddReview},
    {path: '/profile', component:Profile},
    {path: '/other/:username', name:'others', component: Other},
    {path: '/notification', component: Notify},
    {path: '/forgot', component: Forgot},
    {path: '/404', component: NotFound},
    {path: '*', redirect: '/404'}
  ],
  mode: "history"
});
