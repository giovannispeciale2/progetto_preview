const Genres = {
  template: `
    <div class="body">
      <headerbar :key="headerKey"></headerbar>
      <main>
        <div class="main">
          <shortbar></shortbar>
          <div class="main-body">
            <!-- Genres -->
            <div class="cards-container">
              <div class="card-section-container">
                <div class="card-section-title-container">
                  <div class="card-section-title" v-if="results.length > 0">
                    Risultati per: "{{category}} ({{genre}})"
                  </div>
                  <div class="card-section-title" v-if="results.length == 0">
                    Nessun risultato trovato per il genere {{genre}} in {{category}}.
                  </div>
                </div>
                <div v-for="result in results" class="card" :style="setBorderColor(result.color)">
                  <div class="card-container">
                    <div class="card-image-container">
                      <router-link :to="{ name:'review', params: {review: result.id, work: result.workId}}">
                        <img class="card-image" v-bind:src="result.workImg"/>
                      </router-link>
                    </div>
                    <div class="card-review-title">
                      <router-link :to="{ name:'review', params: {review: result.id, work: result.workId}}">
                        <a :title="result.reviewTitle">
                          {{result.reviewTitle | limit(15)}}
                        </a>
                      </router-link>
                    </div>
                    <div class="card-work-title" :title="result.workTitle">
                      <!--<router-link :to="{ name:'work', params: {work: result.workId}}">
                        <a :title="result.workTitle">-->
                          {{result.workTitle | limit(20)}}
                        <!--</a>
                      </router-link>-->
                    </div>
                    <div class="card-user">
                      <router-link :to="{ name:'others', params: {username: result.userNickname}}" class="link">
                        <a :title="result.userNickname">
                          <div class="card-user-container">
                            <div class="card-user-avatar">
                              <img :src="result.userImg">
                            </div>
                            <div class="card-user-nickname" :title="result.userNickname">
                              {{result.userNickname | limit(16)}}
                            </div>
                          </div>
                        </a>
                      </router-link>
                    </div>
                    <div class="card-rating">
                      <div class="rating-value">
                        {{result.reviewAvgRate}}
                      </div>
                      <div class="ratings">
                        <div class="empty-stars"></div>
                        <div class="full-stars" :style="renderRating(result.reviewAvgRate)"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Genres End -->
          </div>
        </div>
      </main>
    </div>
  `,
   data(){
     return {
       categories: [],
       category: "",
       results: [],
       genre: "",
       color: "",
       headerKey: 0,
     }
   },
   watch: {
     '$route.params.categoria': function(categoryVal){
       this.category = categoryVal;
       this.getReviews();
     },
     '$route.params.genere': function(genreVal){
       this.genre = genreVal;
       this.getReviews();
     },
   },
   mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
    })
     this.init();
     this.getReviews();
   },
   methods: {
    resize() {
      this.headerKey++;
    },
     init: function(){
       this.category = this.$route.params.categoria;
       this.genre = this.$route.params.genere;
       axios.get('/api/categories/')
       .then(response =>{
         response.data.forEach(category => {
           this.categories.push(category);
           if(category.name === this.category)
              this.color = response.data.opacity;
         });
       });
     },
     getReviews: function(){
       this.results = [];
       axios.get('/api/reviews')
       .then(response => {
         response.data.forEach(review => {
           axios.get('/api/works/' + review.work)
           .then(responseWork =>{
             if(responseWork.data.category === this.category && responseWork.data.genre === this.genre){
               axios.get('/api/users/' + review.user)
               .then(responseUser => {
                 var renderedResult = {};
                 renderedResult.id = review._id;
                 renderedResult.reviewTitle = review.title;
                 renderedResult.reviewText = review.text;
                 renderedResult.reviewDate = review.date;
                 renderedResult.reviewAvgRate = review.avg.toFixed(2);
                 renderedResult.workId = review.work;
                 renderedResult.workTitle = responseWork.data.title;
                 renderedResult.workImg = responseWork.data.img;
                 renderedResult.userId = review.user;
                 renderedResult.userImg = responseUser.data.img;
                 renderedResult.category = responseWork.data.category;
                 renderedResult.userNickname = responseUser.data.nickname;
                 for(i = 0; i < this.categories.length; i++) {
                   if(this.categories[i].name == renderedResult.category)
                     renderedResult.color = this.categories[i].color;
                 }
                 this.results.push(renderedResult);
               })
               .catch(error => console.log(error));
             }
           });
         });

       });
     },
     renderRating: function(avg) {
       var perc = avg * 100 / 5;
       return "width: " + perc + "%";
     },
     setBorderColor: function(color) {
       return "border: solid 3px " + color + ";";
     },
   },
   filters: {
     limit: function(text, length) {
       if(text.length > length - 3)
         return text.substring(0, length-3)+"...";
       else
         return text;
     }
   }
}
