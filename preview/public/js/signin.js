const Signin = {
  template: `
		<!-- Main Body -->
    <div class="body">
      <headerbar :key="headerKey"></headerbar>
      <main>
        <div class="main">

          <!-- Sign In -->
          <div class="log">
            <div class="login-box" style="height: 80%;">
              <div class="login-imgLocked-container">
      			   <img src="static/images/previewNewLogin.png" class="avatar">
              </div>
        			<div class="signin-nickname-container">
        		    <input class="nickname" v-model="signUpData.nickname" id="nickname" type="text" placeholder="Nome utente" required/>
        			</div>
              <div class="signin-sex-container">
                <div class="signin-sex-radio-container">
                  <div><input v-model="signUpData.sex" type="radio" name="maschio" value="Maschio"/></div>
                  <div class="signin-sex-radio-name-container">Maschio</div>
                </div>
                <div class="signin-sex-radio-container">
                  <div><input v-model="signUpData.sex" type="radio" name="femmina" value="Femmina"/></div>
                  <div class="signin-sex-radio-name-container">Femmina</div>
                </div>
        			</div>
        			<div class="signin-password-container">
        		    <input class="signinPassword" v-model="signUpData.password" id="password" type="password" placeholder="Password" required/>
                <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password" @click.prevent="togglePassword"></span>
        			</div>
        			<div class="signin-verifypww-container">
        		    <input class="signinPassword" v-model="verifyPassword" id="verpassword" type="password" placeholder="Conferma password" required/>
                <span toggle="#verpassword" class="fa fa-fw fa-eye field-icon toggle-verifyPassword" @click.prevent="toggleVerifyPassword"></span>
        			</div>
        			<div class="signin-email-container">
        				<input class="mail" v-model="signUpData.mail" type="text" id="mail" placeholder="Mail" required/>
        			</div>
              <button class="login-buttonLogin" @click.prevent = "signin" type="submit" :disabled="disabled()">Avanti</button>

              <div v-if="notMatching" class="login-wrong-credentials">
                Password non corrispondenti.
              </div>

              <div v-if="wrongNickname" class="login-wrong-credentials">
                Il nome utente è già in uso.
              </div>

              <div v-if="wrongMail" class="login-wrong-credentials">
                La mail è già in uso.
              </div>

              <hr class="login-box-line">

              <div class="login-signin-box">
                <div>Hai già un account? <a class="login-signin-link" @click.prevent = "login">Accedi</a></div>
              </div>

            </div>
          </div>
          <!-- Alert modal -->
          <div class="modal fade" id="signinModal" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Registrazione</h4>
                </div>
                <div class="modal-body alert-body">
                  <p>Registrazione avvenuta con successo.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="profile-loginButtonMenù" data-dismiss="modal" @click.prevent="login">Accedi</button>
                </div>
              </div>
              
            </div>
          </div>
          <!-- Alert modal end -->
          
          <!-- Sign In End -->
        </div>
      </main>
      <!-- Main Body End -->
      <div id="push" style="height: 0px !important;"></div>
      <footer-component></footer-component>
    </div>
  `,
  data() {
    return{
      signUpData: {
        "nickname": "",
        "password": "",
        "mail": "",
        "sex": "",
        "img": ""
      },
      verifyPassword: "",
      matching: false,
      notMatching: false,
      wrongNickname: false,
      wrongMail: false,
      headerKey: 0,
    }
  },
  watch: {
    verifyPassword: function(val, oldVal) {
      if(val == this.signUpData.password && val == "") {
        this.matching = false;
        this.notMatching = false;
      } else if(val != this.signUpData.password) {
        this.matching = false;
        this.notMatching = true;
      } else {
        this.matching = true;
        this.notMatching = false;
      }
    },
    'signUpData.password': function(val, oldVal) {
      if(this.notMatching || this.matching) {
        if(val == this.verifyPassword && val == "") {
          this.matching = false;
          this.notMatching = false;
        } else if(val != this.verifyPassword) {
          this.matching = false;
          this.notMatching = true;
        } else {
          this.matching = true;
          this.notMatching = false;
        }
      }
    },
    'signUpData.nickname': function(val, oldVal) {
      if(this.wrongNickname)
        this.wrongNickname = false;
    },
    'signUpData.mail': function(val, oldVal) {
      if(this.wrongMail)
        this.wrongMail = false;
    },
  },
  mounted() {},
  methods: {
    togglePassword() {
      $(".toggle-password").toggleClass("fa-eye fa-eye-slash");
      var input = $($(".toggle-password").attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    },
    toggleVerifyPassword() {
      $(".toggle-verifyPassword").toggleClass("fa-eye fa-eye-slash");
      var input = $($(".toggle-verifyPassword").attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    },
    signin() {
      axios.post("/api/users", this.signUpData)
        .then(response => {
          if(response.data.error != undefined) {
            if(response.data.error == "Nickname Already Exists")
              this.wrongNickname = true;
            else if (response.data.error == "Email Already Exists")
              this.wrongMail = true;
          } else {
            $('#signinModal').modal('show');
          }
      });
    },
    disabled() {
      if(this.notMatching)
        return true;
      else if(this.signUpData.nickname == ""
              || this.signUpData.password == ""
              || this.verifyPassword == ""
              || this.signUpData.mail == ""
              || this.signUpData.sex == "")
        return true;
      else
        return false;
    },
    login() {
      this.$router.push('login');
    }
  }
}
