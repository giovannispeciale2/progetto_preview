const Login = {
  template: `
  <div class="body">
    <headerbar :key="headerKey"></headerbar>
    <main>
      <div class="main">
          <!-- Login -->
          <div class="log">
            <div class="login-box" :style="loginBoxHeight">
              <div class="login-imgLocked-container">
                <img class="avatar" src="static/images/previewNewLogin.png" alt="Avatar" >
              </div>
              <div class="login-nickname-container">
                <input class="nickname" v-model="loginData.nickname" id="nickname" type="text" placeholder="Inserisci nome utente" required @keyup.enter="login"/>
              </div>
              <div class="login-password-container">
                <input v-model="loginData.password" id="password" type="password" placeholder="Inserisci password" required @keyup.enter="login" class="loginPassword"/>
                <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password" @click.prevent="togglePassword"></span>
              </div>
              <button class="login-buttonLogin" @click.prevent = "login" type="submit" :disabled="disabled()">Accedi</button>
              <div class="login-checkbox-container">
                <label class="login-label-remember">
                  <div class="login-remember-checkbox"><input type="checkbox" name="remember" v-model="remember"></div>
                  <div>Ricordami</div>
                </label>
                <label class="login-label-forgot">
                  <span class="password"><a @click.prevent=forgot>Hai dimenticato la password?</a></span>
                </label>
              </div>

              <div v-if="wrong" class="login-wrong-credentials">
                Spiacenti, il nome utente o la password non erano corretti. Ritenta.
              </div>

              <hr class="login-box-line">

              <div class="login-signin-box">
                <div>Non hai un account? <a class="login-signin-link" @click.prevent = "signIn">Iscriviti</a></div>
              </div>
            </div>
          </div>
          <!-- Login End -->
      </div>
    </main>
    <div id="push" style="height: 0px !important;"></div>
    <footer-component></footer-component>
  </div>
  `,
  data() {
    return {
      loginData: {
        "nickname": "",
        "password": ""
      },
      remember: true,
      wrong: false,
      loginBoxDefaultHeight: "height: 58%;",
      loginBoxHeight: "height: 58%;",
      loginBoxWrongHeight: "height: 72%;",
      headerKey: 0,
    }
  },
  mounted() {
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
    })
    this.control_Cookies();
  },
  methods: {
    resize() {
      this.headerKey++;
    },
    togglePassword() {
      $(".toggle-password").toggleClass("fa-eye fa-eye-slash");
      var input = $($(".toggle-password").attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    },
    forgot(){
      this.$router.push('/forgot');
    },
    control_Cookies(){
      var coockie = this.$cookies.get("user_session");
      if(coockie !== null && coockie !== undefined) {
        sessionStorage.setItem("user_session", coockie);
        this.$router.push("/");
      }
    },
    disabled() {
      if(this.loginData.nickname == "" || this.loginData.password == "")
        return true;
      else
        return false;
    },
    login() {
      if(this.loginData.nickname !== "" && this.loginData.password !== "")
        axios.get("/api/login", {
          params: {
            nickname: this.loginData.nickname,
            password: this.loginData.password
          }
        })
        .then(response => {
          if(!response.data.error){
            sessionStorage.setItem("user_session", response.data);
            if(this.remember)
              this.$cookies.set("user_session", response.data, { expires: '1D' });
            window.location.replace("/");
          }
          else {
            this.wrong = true;
            this.loginBoxHeight = this.loginBoxWrongHeight;
          }
        })
        .catch(error => console.log(error));
    },
    signIn() {
      this.$router.push('signin');
    },
  }
}
