const Profile = {
  template: `
  <div class="body">
    <headerbar :key="headerKey"></headerbar>
    <main>
      <div class="main">
        <shortbar></shortbar>
        <div class="main-body">
          <!-- Profile -->

          <div class="container-profile-user">

            <div class="wrapper-data-user">
              <div class="container-data-user">
                <div class="img-avatar-container img-avatar-container-profile">
                  <img :src="user.img" alt="Avatar" @click.prevent="showUserDataAvatar" class="user-img-avatar-profile" ref="profileImage" id="profileImage">
                  <div class="overlay-profile-avatar" @click.prevent="showUserDataAvatar" ref="profileOverlay" id="profileOverlay">
                    <a class="icon-profile-avatar">
                      <i class="fas fa-camera"></i>
                    </a>
                  </div>
                </div>
                <div class="container-general-user">
                  <div class="container-general-user-topRow">
                    <div class="container-user-nickname">
                      {{user.username}}
                    </div>

                    <div class="container-profile-userBadges" v-if="user.badges.length > 0">
                      <router-link :to="{ name:'badgeBoard', params: {user: user.id}, title: 'Achievements'}">
                        <div class="inner-badges-container">
                          <div class="container-badge-user" v-for="badge in user.badges">
                            <img :src="badge" alt="Badge" id="badge_user">
                          </div>
                        </div>
                      </router-link>
                    </div>

                    <div class="container-menu-user">
                      <button title="Achievements" class="user-menu-buttons" @click.prevent="goToInfoBadges">
                        <i class="fas fa-award"></i>
                      </button>
                      <button title="Impostazioni" class="user-menu-buttons" @click.prevent="toggleAvatarDropdownMenuUser">
                        <i class="fas fa-cog"></i>
                      </button>
                      <button title="Analisi" class="user-menu-buttons" data-toggle="modal" data-target="#myModal">
                        <i class="fas fa-chart-pie"></i>
                      </button>
                      <div v-if="activeDropdownMenuUser" class="activeDropdownMenuUser">
                        <div class="dropdown-line-wrapper" @click.prevent="showUserData">
                          <div class="profile-dropdown-line">
                            <div class="profile-dropdown-icon">
                              <i class="fas fa-edit"></i>
                            </div>
                            <div class="profile-dropdown-text">
                              Modifica profilo
                            </div>
                          </div>
                        </div>
                        <hr class="line"/>
                        <div class="dropdown-line-wrapper" @click.prevent="logout">
                          <div class="profile-dropdown-line">
                            <div class="profile-dropdown-icon">
                              <i class="fas fa-sign-out-alt"></i>
                            </div>
                            <div class="profile-dropdown-text">
                              Esci
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="container-modal-analysis">
                        <!-- Modal Analisi -->
                        <div class="modal fade" id="myModal" role="dialog">
                          <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Analisi Recensioni</h4>
                              </div>

                              <div class="modal-body">
                                <div class="graphics-container">
                                  <div class="title-chart">
                                    Recensioni Create
                                  </div>
                                  <div id="chart">
                                    <apexchart type=pie width=380 :options="chartOptions" :series="series" />
                                  </div>
                                  <div class="title-chart">
                                    Voti Ottenuti
                                  </div>
                                  <div id="chart2">
                                    <apexchart type=pie width=380 :options="chartOptions" :series="series" />
                                  </div>
                                </div>
                              </div>

                              <div class="modal-footer">
                                <button type="button" class="profile-closeButtonMenù" data-dismiss="modal">Chiudi</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="container-general-user-bottomRow">
                    <div class="container-statistics-points">
                      <span>{{user.points}}</span> punti
                    </div>
                    <div class="container-statistics-numReviews">
                      <span>{{user.nReview}}</span> recensioni
                    </div>
                    <div class="container-statistics-avg">
                      <span>{{user.avg.toFixed(2)}}</span> media
                    </div>
                  </div>


                </div>
              </div>
            </div>


            <!-- Edit profile section -->

            <div class="wrapper-edit-profile" v-if="ableModify">
              <div class="container-profile-modifyData">
                <div class="container-profile-modifyData-title">
                  Modifica informazioni Utente:
                </div>
                <div v-if="wrongEditProfile" class="wrongEditProfile">
                  {{this.errorMessage}}
                </div>
                <div class="container-profile-modifyDataNickname">
                  <input class="editBar" type="text" name="Nickname" v-model="userEdited.username">
                </div>
                <div class="container-profile-modifyDataOldPassword">
                  <input class="editBar" id="oldpassword" type="password" name="Vecchia Password" v-model="userEdited.oldPsw" placeholder="Conferma Password Attuale">
                  <span toggle="#oldpassword" class="fa fa-fw fa-eye field-icon toggle-oldPsw" @click.prevent="toggleOldPassword"></span>
                </div>
                <div class="container-profile-modifyDataNewPassword">
                  <input v-model="userEdited.password" id="newpassword" class="editBar" type="password" name="Nuova Password" placeholder="Inserisci Nuova Password">
                  <span toggle="#newpassword" class="fa fa-fw fa-eye field-icon toggle-password" @click.prevent="toggleNewPassword"></span>
                </div>
                <div class="container-profile-modifyDataEmail">
                  <input class="editBar" type="text" name="Email" v-model="userEdited.email">
                </div>


                <cropperuser @uploaded:img="imgName"></cropperuser>

                <div class="container-buttonsModify">
                  <div class="edit-profile-button-wrapper">
                    <input class="buttonModify" @click.prevent="editData" title="Modifica" value="Modifica" type="submit"/>
                  </div>
                  <div class="edit-profile-button-wrapper">
                    <input class="buttonCloseModify" @click.prevent="closeData" title="Chiudi" value="Chiudi" type="submit"/>
                  </div>
                </div>
              </div>
            </div>



            <!-- Reviews Section -->
            <div class="profile-user-notReviews" v-if="this.userReviews.length == 0">
              <div class="card-section-title profile-card-section-title">
                Mie Recensioni
              </div>
              <div class="notReviews-title">
                Non hai ancora pubblicato recensioni!
              </div>
            </div>
            <div class="card-section-container user-reviews-section" v-if="this.userReviews.length > 0">
              <div class="card-section-title profile-card-section-title">
                Mie Recensioni
              </div>
              <div v-for="result in userReviews" class="card" :style="setBorderColor(result.color)">
                <div class="card-container">
                  <div class="card-image-container">
                    <router-link :to="{ name:'review', params: {review: result.id, work: result.workId}}">
                      <img class="card-image" v-bind:src="result.workImg"/>
                    </router-link>
                  </div>
                  <div class="card-review-title">
                    <router-link :to="{ name:'review', params: {review: result.id, work: result.workId}}">
                      <a :title="result.reviewTitle">
                        {{result.reviewTitle | limit(15)}}
                      </a>
                    </router-link>
                  </div>
                  <div class="card-work-title" :title="result.workTitle">
                    <!--<router-link :to="{ name:'work', params: {work: result.workId}}">
                      <a :title="result.workTitle">-->
                        {{result.workTitle | limit(20)}}
                      <!--</a>
                    </router-link>-->
                  </div>
                  <div class="card-user">
                    <router-link :to="{ name:'others', params: {username: result.userNickname}}" class="link">
                      <a :title="result.userNickname">
                        <div class="card-user-container">
                          <div class="card-user-avatar">
                            <img :src="user.img">
                          </div>
                          <div class="card-user-nickname" :title="result.userNickname">
                            {{result.userNickname | limit(16)}}
                          </div>
                        </div>
                      </a>
                    </router-link>
                  </div>
                  <div class="card-rating">
                    <div class="rating-value">
                      {{result.reviewAvgRate}}
                    </div>
                    <div class="ratings">
                      <div class="empty-stars"></div>
                      <div class="full-stars" :style="renderRating(result.reviewAvgRate)"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Profile End -->

          <!-- Edit modal -->
          <div class="modal fade" id="editModal" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Modifica profilo</h4>
                </div>
                <div class="modal-body alert-body">
                  <p>Dati modificati con successo.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="profile-closeButtonMenù" data-dismiss="modal" @click.prevent="closeModal">Chiudi</button>
                </div>
              </div>
              
            </div>
          </div>
          <!-- Edit modal end -->

          </div>
        </div>
      </main>
    </div>
  `,
  data(){
    return{
      categories: [],
      activeDropdownMenuUser: false,
      user:{
        id: "",
        username: "",
        password: "",
        email: "",
        badges: [],
        img: "",
        nReview: 0,
        points: 0,
        avg: 0,
        nvotes: 0,
        sum: 0,
        ratedReviews: 0,
      },
      userEdited: {},
      userReviews: [],
      isAdmin: false,
      ableModify: false,
      smodal: false,
      dataReviews: [],
      dataLike: [],
      optionReview: {},
      optionLike: {},
      wrongEditProfile: false,
      pushHeight: '',
      errorMessage: "",
      headerKey: 0,
    }
  },
  mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
      this.init();
    })
  },
  methods: {
    resize() {
      setTimeout(() => {
        this.headerKey++;
        var pushSize = this.getPushSize();
        this.pushHeight = "height: "
            + parseFloat(pushSize)
            + "px";
      }, 100);
    },
    toggleOldPassword() {
      $(".toggle-oldPsw").toggleClass("fa-eye fa-eye-slash");
      var input = $($(".toggle-oldPsw").attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    },
    toggleNewPassword() {
      $(".toggle-password").toggleClass("fa-eye fa-eye-slash");
      var input = $($(".toggle-password").attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    },
    toggleAvatarDropdownMenuUser() {
      this.activeDropdownMenuUser = !this.activeDropdownMenuUser;
    },
    imgName(string) {
      this.userEdited.img = string;
    },
    init(){
      this.getUser();
      this.listCategories();
      this.resize();
    },
    show() {
      this.smodal = true;
    },
    hide() {
      this.smodal = false;
    },
    listCategories() {
      axios.get("/api/categories")
      .then(response => this.categories = response.data)
      .catch(err => console.log(err));
    },
    getUser(){
      var userId = sessionStorage.getItem("user_session");
      axios.get("/api/users/" + userId)
      .then(result => {
        this.user.id = userId;
        this.user.username = result.data.nickname;
        this.user.email = result.data.mail;
        this.user.img = result.data.img;
        this.user.badges = this.clearDoubleBadge(result.data.badges);
        this.user.points = result.data.points;
        this.user.nvotes = result.data.votes;
        this.user.nReview = this.userReviews.length;
        this.isAdmin = result.data.admin;
        this.userAvatar();
        this.getUserData();
        this.userEdited.username = result.data.nickname;
        this.userEdited.email = result.data.mail;
        this.userEdited.img = result.data.img;
      });
    },
    clearDoubleBadge(badges){
      var badgeToSave = [];
      badges.forEach(badge => {
        if(!badgeToSave.includes(badge))
          badgeToSave.push(badge);
      });
      return badgeToSave;
    },
    getUserData(){
      var userId = sessionStorage.getItem("user_session");
      axios.get("/api/reviews/user/" + userId)
      .then(response => {
        var length = response.data.length;
        if(length === 0)
          this.composeChart();
        else
          response.data.forEach(review => {
            axios.get('/api/works/' + review.work)
            .then(responseWork => {
              var renderedResult = {};
              this.user.nReview ++;
              renderedResult.id = review._id;
              renderedResult.reviewTitle = review.title;
              renderedResult.reviewText = review.text;
              renderedResult.reviewDate = review.date;
              renderedResult.workId = review.work;
              renderedResult.reviewAvgRate = review.avg.toFixed(2);
              renderedResult.workTitle = responseWork.data.title;
              renderedResult.workImg = responseWork.data.img;
              renderedResult.userId = userId;
              renderedResult.category = responseWork.data.category;
              renderedResult.genre =  responseWork.data.genre;
              renderedResult.userNickname = this.user.username;
              renderedResult.points = review.points;
              renderedResult.rates = review.rates;
              renderedResult.color = "";
              for(i = 0; i < this.categories.length; i++) {
                if(this.categories[i].name == renderedResult.category)
                  renderedResult.color = this.categories[i].color;
              }
              this.userReviews.push(renderedResult);

              if(renderedResult.reviewAvgRate > 0) {
                this.user.sum += parseFloat(renderedResult.reviewAvgRate);
                this.user.ratedReviews++;
              }
              if(this.user.nReview === length)
                this.composeChart();
            });
          });
      });
      this.pushFooter();
    },
    showUserData() {
      // $(".collapse").collapse('toggle');
      this.toggleAvatarDropdownMenuUser();
      this.showUserDataAvatar();
    },
    showUserDataAvatar() {
      if(this.ableModify == false) {
        this.ableModify = true;
        this.pushFooter();
      }
      else if(this.ableModify == true) {
        this.ableModify = false;
        this.pushFooter();
      }
    },
    userAvatar(){
      if(!this.user.img){
        var avatar = document.getElementById("profile_image");
        avatar.src = "static/images/avatar_vuoto.jpg";
      }
    },
    logout(){
      this.$router.push("/logout");
    },
    editData(){
      if(this.userEdited.oldPsw == undefined || this.userEdited.oldPsw == "") {
        this.errorMessage = "E' necessario inserire la password attuale per modificare i dati";
        this.wrongEditProfile = true;
      }
      else {
        this.wrongEditProfile = false;
        var update = {
          idU: sessionStorage.getItem("user_session"),
          name: this.userEdited.username,
          oldPsw: this.userEdited.oldPsw,
          password: this.userEdited.password,
          email: this.userEdited.email,
          img: this.userEdited.img
        };
        axios.post("/api/user/edit", update)
          .then(response => {
            if(response.data.error === "Vecchia password non corrispondente! Riprova."
                || response.data.error === "Non puoi inserire la stessa password") {
              this.errorMessage = response.data.error;
              this.wrongEditProfile = true;
            }
            else {
              this.wrongEditProfile = false;
              $('#editModal').modal('show');
            }
          });
      }
    },
    closeModal() {
      this.$router.go();
    },
    closeData(){
      this.ableModify = false;
    },
    setBorderColor: function(color) {
      return "border: solid 3px " + color + ";";
    },
    renderRating: function(avg) {
      // avg : 5 = x : 100
      var perc = avg * 100 / 5;
      return "width: " + perc + "%";
    },
    avg(){
      if(this.user.ratedReviews == 0)
        this.user.avg = 0;
      else
        this.user.avg = this.user.sum / this.user.ratedReviews;
      if(this.userReviews.length == 0)
        this.user.nReview = 0;
    },
    // VA IN 404 ANZICHE' IN BADGEBOARD
    goToInfoBadges() {
      this.$router.push({name:'badgeBoard', params: {user: this.user.id}});
    },
    pushFooter() {
      var oldPushHeight = this.pushHeight.substring(7, this.pushHeight.length - 2);
      var newPushHeight = "height: " + (parseFloat(oldPushHeight) + parseFloat(this.getPushSize())) + "px";
      this.pushHeight = newPushHeight;
    },
    getPushSize() {
      var w = window.innerWidht;
      w = 1400;
      if (w > 1400)
        return 365;
      else if (w > 1000)
        return 323;
      else if (w > 900)
        return 301;
      else if (w > 800)
        return 280;
      else if (w > 700)
        return 259;
      else if (w > 650)
        return 238;
      else if (w > 550)
        return 301;
      else
        return 280;
    },
    composeChart(){
      this.avg();
      this.editorData();
      this.optionComposer(this.dataReviews, this.dataLike);
      var chart = new ApexCharts(document.querySelector("#chart"), this.optionReview);
      var chart2 = new ApexCharts(document.querySelector("#chart2"), this.optionLike);
      chart.render();
      chart2.render();
    },
    editorData(){
      var film = this.userReviews.filter(rev => rev.category === "Film/Serie TV");
      var giochi = this.userReviews.filter(rev => rev.category === "Giochi");
      var libri = this.userReviews.filter(rev => rev.category === "Libri");
      this.dataReviews.push(film.length);
      this.dataReviews.push(libri.length);
      this.dataReviews.push(giochi.length);

      var movieVotes = film.reduce((accumulator, rev) =>{
        return accumulator + rev.rates.length;
      }, 0);
      this.dataLike.push(movieVotes);
      var bookVotes = libri.reduce((accumulator, rev) =>{
        return accumulator + rev.rates.length;
      }, 0);
      this.dataLike.push(bookVotes);
      var gameVotes = giochi.reduce((accumulator, rev) =>{
        return accumulator + rev.rates.length;
      }, 0);
      this.dataLike.push(gameVotes);
    },
    sameArray(a, b){
      if(a.length !== b.length)
        return false;
      for(i = 0; i < a.length; i++){
        if(a[i] !== b[i])
          return false
      }
      return true
    },
    optionComposer(reviews, likes) {
      var empty = [0,0,0];
      var emptyColors = ['rgb(91,157,216)', 'rgb(117,192,76)', 'rgb(232,95,92)', '#696969'];
      var emptyLabels = ['Film/Serie TV', 'Libri', 'Giochi', 'Nessuna'];

      var baseColors = ['rgb(91,157,216)', 'rgb(117,192,76)', 'rgb(232,95,92)'];
      var baseLabels = ['Film/Serie TV', 'Libri', 'Giochi'];

      var color = baseColors;
      var label = baseLabels;
      if(this.sameArray(reviews, empty)){
        color = emptyColors;
        label = emptyLabels;
        reviews.push(1);
      }

      this.optionReview = {
       chart: {
           width: 380,
           type: 'pie',
       },
       labels: label,
       colors: color,
       fill: {
        colors: color
       },
       series: reviews,
       responsive: [{
           breakpoint: 480,
           options: {
               chart: {
                   width: 200
               },
               legend: {
                   position: 'bottom'
               }
           }
       }]
     };

     var color = baseColors;
     var label = baseLabels;
     if(this.sameArray(likes, empty)){
       color = emptyColors;
       label = emptyLabels;
       likes.push(1);
     }

     this.optionLike = {
      chart: {
          width: 380,
          type: 'pie',
      },
      labels: label,
      colors: color,
      fill: {
       colors: color
      },
      series: likes,
      responsive: [{
          breakpoint: 480,
          options: {
              chart: {
                  width: 200
              },
              legend: {
                  position: 'bottom'
              }
          }
        }]
      };
    }
  },
  filters: {
    limit: function(text, length) {
      if(text.length > length - 3)
        return text.substring(0, length-3)+"...";
      else
        return text;
    }
  }
}
