const Logout = {
  mounted(){
    this.logout();
  },
  methods: {
    logout: function() {
      this.$cookies.remove("user_session");
      sessionStorage.removeItem("user_session");
      window.location.replace("/");
    },
  }
}
