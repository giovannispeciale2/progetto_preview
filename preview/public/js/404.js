const NotFound = {
  template: `
  <div class="body">
    <main>
      <div class="body-not-found">
        <div class="notFound">
          <img src="static/images/404.png"/>
        </div>
        <div class="not-found-button-container">
          <button class="go-back-button" @click.prevent="back"> <i class="fas fa-angle-left arrow-icon"></i> Go back home</button>
        </div>
      </div>
    </main>
  </div>
  `,
  data() {
    return {}
  },
  mounted() {
    this.control_Cookies();
  },
  methods: {
    control_Cookies(){
      var coockie = this.$cookies.get("user_session");
      if(coockie !== null && coockie !== undefined) {
        sessionStorage.setItem("user_session", coockie);
      }
    },
    back() {
      this.$router.push('/');
    }
  }
}
