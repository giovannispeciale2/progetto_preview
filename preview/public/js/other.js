const Other = {
  template: `
  <div class="body">
    <headerbar :key="headerKey"></headerbar>
    <main>
      <div class="main">
        <shortbar></shortbar>
        <div class="main-body">
          <!-- Profile -->
          <div class="container-profile-user">
            <div class="wrapper-data-user">
              <div class="container-data-user">
                <div class="img-avatar-container">
                  <img :src="other.img" alt="Avatar" id="profile_image" class="user-img-avatar-profile">
                </div>
                <div class="container-general-user">
                  <div class="container-general-user-topRow">
                    <div class="container-user-nickname">
                      {{other.username}}
                    </div>

                    <div class="container-profile-userBadges">
                      <router-link :to="{ name:'badgeBoard', params: {user: other.id}, title: 'Achievements'}">
                        <div class="inner-badges-container">
                          <div class="container-badge-user" v-for="badge in other.badges">
                            <img :src="badge" alt="Badge" id="badge_user">
                          </div>
                        </div>
                      </router-link>
                    </div>

                    <div v-bind:class="classButtons()"> <!--class="container-menu-user-other"-->
                      <button title="Achievements" class="user-menu-buttons" @click.prevent="goToInfoBadges">
                        <i class="fas fa-award"></i>
                      </button>
                      <button title="Impostazioni" class="user-menu-buttons" @click.prevent="toggleAvatarDropdownMenuOther" v-if="isAdmin">
                        <i class="fas fa-cog"></i>
                      </button>
                      <button title="Analisi" class="user-menu-buttons" data-toggle="modal" data-target="#myModal">
                        <i class="fas fa-chart-pie"></i>
                      </button>
                      <div v-if="activeDropdownMenuOther" class="activeDropdownMenuUser">
                        <div class="dropdown-line-wrapper" @click.prevent="banUser">
                          <div class="profile-dropdown-line">
                            <div class="profile-dropdown-icon">
                              <i class="fas fa-ban"></i>
                            </div>
                            <div class="profile-dropdown-text">
                              Ban
                            </div>
                          </div>
                        </div>
                        <hr class="line"/>
                      </div>
                      <div class="container-modal-analysis">
                        <!-- Modal Analisi -->
                        <div class="modal fade" id="myModal" role="dialog">
                          <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Analisi Recensioni</h4>
                              </div>

                              <div class="modal-body">
                                <div class="graphics-container">
                                  <div class="title-chart">
                                    Recensioni Create
                                  </div>
                                  <div id="chart">
                                    <apexchart type=pie width=380 :options="chartOptions" :series="series" />
                                  </div>
                                  <div class="title-chart">
                                    Voti Ottenuti
                                  </div>
                                  <div id="chart2">
                                    <apexchart type=pie width=380 :options="chartOptions" :series="series" />
                                  </div>
                                </div>
                              </div>

                              <div class="modal-footer">
                                <button type="button" class="profile-closeButtonMenù" data-dismiss="modal">Chiudi</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="container-general-user-bottomRow">
                    <div class="container-statistics-points">
                      <span>{{other.points}}</span> punti
                    </div>
                    <div class="container-statistics-numReviews">
                      <span>{{other.points}}</span> punti
                    </div>
                    <div class="container-statistics-avg">
                      <span>{{other.points}}</span> punti
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="profile-user-notReviews" v-if="this.otherReviews.length == 0">
              <div class="card-section-title profile-card-section-title">
                Recensioni di {{other.username}}
              </div>
              <div class="notReviews-title">
                Questo utente non ha ancora pubblicato recensioni!
              </div>
            </div>
            <div class="card-section-container user-reviews-section" v-if="otherReviews.length > 0">
              <div class="card-section-title profile-card-section-title">
                Recensioni di {{other.username}}
              </div>
              <div v-for="result in otherReviews" class="card" :style="setBorderColor(result.color)">
                <div class="card-container">
                  <div class="card-image-container">
                    <router-link :to="{ name:'review', params: {review: result.id, work: result.workId}}">
                      <img class="card-image" v-bind:src="result.workImg"/>
                    </router-link>
                  </div>
                  <div class="card-review-title">
                    <router-link :to="{ name:'review', params: {review: result.id, work: result.workId}}">
                      <a :title="result.reviewTitle">
                        {{result.reviewTitle | limit(15)}}
                      </a>
                    </router-link>
                  </div>
                  <div class="card-work-title" :title="result.workTitle">
                    <!--<router-link :to="{ name:'work', params: {work: result.workId}}">
                      <a :title="result.workTitle">-->
                        {{result.workTitle | limit(20)}}
                      <!--</a>
                    </router-link>-->
                  </div>
                  <div class="card-user">
                    <router-link :to="{ name:'others', params: {username: result.userNickname}}" class="link">
                      <a :title="result.userNickname">
                        <div class="card-user-container">
                          <div class="card-user-avatar">
                            <img :src="other.img">
                          </div>
                          <div class="card-user-nickname" :title="result.userNickname">
                            {{result.userNickname | limit(16)}}
                          </div>
                        </div>
                      </a>
                    </router-link>
                  </div>
                  <div class="card-rating">
                    <div class="rating-value">
                      {{result.reviewAvgRate}}
                    </div>
                    <div class="ratings">
                      <div class="empty-stars"></div>
                      <div class="full-stars" :style="renderRating(result.reviewAvgRate)"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- Profile End -->
          <!-- Alert modal -->
          <div class="modal fade" id="otherModal" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Ban Utente</h4>
                </div>
                <div class="modal-body alert-body">
                  <p>{{alertText}}</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="profile-closeButtonMenù" data-dismiss="modal" @click.prevent="closeModal">Chiudi</button>
                </div>
              </div>
              
            </div>
          </div>
          <!-- Alert modal end -->
          </div>
        </div>
      </main>
      <div id="push" :style="pushHeight"></div>
      <footer-component></footer-component>
    </div>
  `,
  data(){
    return {
      categories: [],
      activeDropdownMenuOther: false,
      other:{
        id:"",
        username: "",
        email: "",
        badges: [],
        img: "",
        nReview: 0,
        points: 0,
        avg: 0,
        nvotes: 0,
        sum: 0,
        ratedReviews: 0,
      },
      user:{
        username: "",
      },
      otherReviews: [],
      isAdmin: false,
      ableModify: false,
      dataReviews: [],
      dataLike: [],
      optionReview: {},
      optionLike: {},
      smodal: false,
      pushHeight: '',
      headerKey: 0,
      alertText: "",
      banError: false,
    }
  },
  watch: {
    '$route.params.username': function(val){
      this.otherReviews = [];
      this.dataReviews = [];
      this.dataLike = [];
      this.other.badges = [];
      this.other.sum = 0;
      this.other.ratedReviews = 0;
      this.init();
    }
  },
  mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
      this.init();
    })
  },
  methods: {
    resize() {
      setTimeout(() => {
        this.headerKey++;
        var pushSize = this.getPushSize();
        this.pushHeight = "height: "
            + parseFloat(pushSize)
            + "px";
      }, 100);
    },
    toggleAvatarDropdownMenuOther() {
      this.activeDropdownMenuOther = !this.activeDropdownMenuOther;
    },
    classButtons() {
      if (this.isAdmin) {
        return "container-menu-user";
      } else {
        return "container-menu-user-other";
      }
    },
    banUser() {
      var ban = {
        idU: this.other,
      };
      axios.post("/api/user/ban", ban)
        .then(response => {
          if(!response.data) {
            this.alertText = "Errore nel bannare l'utente: " + this.other.username;
          } else {
            this.alertText = "Utente " + this.other.username + " bannato con successo!";
          }
          $('#otherModal').modal('show');
        });
    },
    closeModal() {
      if(this.banError)
        this.banError = false;
      else  
        this.$router.push("/");
    },
    init(){
      this.getUser();
      this.getOther();
      this.listCategories();
      this.resize();
    },
    show() {
      this.smodal = true;
    },
    hide() {
      this.smodal = false;
    },
    listCategories() {
      axios.get("/api/categories")
      .then(response => this.categories = response.data)
      .catch(err => console.log(err));
    },
    getOther(){
      var altro = this.$route.params.username;
      axios.get("/api/other/" + altro)
      .then(uId => {
        this.other.id = uId.data;
        axios.get("/api/users/" + this.other.id)
        .then(result => {
          this.other.username = result.data.nickname;
          this.other.email = result.data.mail;
          var arrayBadges = this.clearDoubleBadge(result.data.badges);
          arrayBadges.forEach(badge => {
            this.other.badges.push("/" + badge);
          });
          this.other.img = result.data.img;
          this.other.points = result.data.points;
          this.other.nvotes = result.data.votes;
          this.other.nReview = this.otherReviews;
          this.userAvatar();
          this.getOtherReviews();
        })
        .catch(err => this.$router.push('/404'));
      })
      .catch(err => this.$router.push('/404'));
    },
    clearDoubleBadge(badges){
      var badgeToSave = [];
      badges.forEach(badge => {
        if(!badgeToSave.includes(badge))
          badgeToSave.push(badge);
      });
      return badgeToSave;
    },
    getUser(){
      var userId = sessionStorage.getItem("user_session");
      axios.get("/api/users/" + userId)
      .then(result => {
        if(result.data.nickname !== this.$route.params.username){
          this.user.username = result.data.nickname;
          this.isAdmin = result.data.admin;
        }
        else
          this.$router.replace("/profile");
      });
    },
    getOtherReviews() {//////////////////////////////////////// controllare
      var user = this.other.id;
      this.otherReviews = [];
      axios.get("/api/reviews/user/" + user)
      .then(response => {
        var length = response.data.length;
        if(length === 0)
          this.composeChart();
        else
          response.data.forEach(review => {
            axios.get('/api/works/' + review.work)
            .then(responseWork => {
              var renderedResult = {};
              this.other.nReview ++;
              renderedResult.id = review._id;
              renderedResult.reviewTitle = review.title;
              renderedResult.reviewText = review.text;
              renderedResult.reviewDate = review.date;
              renderedResult.workId = review.work;
              renderedResult.reviewAvgRate = review.avg.toFixed(2);
              renderedResult.workTitle = responseWork.data.title;
              renderedResult.workImg = responseWork.data.img;
              renderedResult.userId = user;
              renderedResult.category = responseWork.data.category;
              renderedResult.genre =  responseWork.data.genre;
              renderedResult.userNickname = this.other.username;
              renderedResult.points = review.points;
              renderedResult.rates = review.rates;

              if(renderedResult.reviewAvgRate > 0) {
                this.other.sum += parseFloat(renderedResult.reviewAvgRate);
                this.other.ratedReviews++;
              }

              for(i = 0; i < this.categories.length; i++) {
                if(this.categories[i].name == renderedResult.category)
                  renderedResult.color = this.categories[i].color;
              }
              this.otherReviews.push(renderedResult);

              if(renderedResult.reviewAvgRate > 0) {
                this.other.sum += parseFloat(renderedResult.reviewAvgRate);
                this.other.ratedReviews++;
              }
              if(this.other.nReview === length){
                this.composeChart();
              }
            });
          });
      });
      this.pushFooter();
    },
    userAvatar(){
      if(!this.other.img){
        var avatar = document.getElementById("profile_image");
        avatar.src = "../static/images/avatar_vuoto.jpg";
      }
    },
    setBorderColor: function(color) {
      return "border: solid 3px " + color + ";";
    },
    renderRating: function(avg) {
      var perc = avg * 100 / 5;
      return "width: " + perc + "%";
    },
    logout(){
      this.$router.push("/logout");
    },
    avg(){
      if(this.other.ratedReviews == 0)
        this.other.avg = 0;
      else
        this.other.avg = this.other.sum / this.other.ratedReviews;
      if(this.otherReviews.length == 0)
        this.other.nReview = 0;
    },
    goToInfoBadges() {
      this.$router.push({name:'badgeBoard', params: {user: this.other.id}});
    },
    pushFooter() {
      var oldPushHeight = this.pushHeight.substring(7, this.pushHeight.length - 2);
      var newPushHeight = "height: " + (parseFloat(oldPushHeight) + parseFloat(this.getPushSize())) + "px";
      this.pushHeight = newPushHeight;
    },
    getPushSize() {
      var w = window.innerWidht;
      w = 1400;
      if (w > 1400)
        return 365;
      else if (w > 1000)
        return 323;
      else if (w > 900)
        return 301;
      else if (w > 800)
        return 280;
      else if (w > 700)
        return 259;
      else if (w > 650)
        return 238;
      else if (w > 550)
        return 301;
      else
        return 280;
    },
    composeChart(){
      this.avg();
      this.editorData();
      this.optionComposer(this.dataReviews, this.dataLike);
      var chart = new ApexCharts(document.querySelector("#chart"), this.optionReview);
      var chart2 = new ApexCharts(document.querySelector("#chart2"), this.optionLike);
      chart.render();
      chart2.render();
    },
    editorData(){
      var film = this.otherReviews.filter(rev => rev.category === "Film/Serie TV");
      var giochi = this.otherReviews.filter(rev => rev.category === "Giochi");
      var libri = this.otherReviews.filter(rev => rev.category === "Libri");
      this.dataReviews.push(film.length);
      this.dataReviews.push(libri.length);
      this.dataReviews.push(giochi.length);

      var movieVotes = film.reduce((accumulator, rev) =>{
        return accumulator + rev.rates.length;
      }, 0);
      this.dataLike.push(movieVotes);
      var bookVotes = libri.reduce((accumulator, rev) =>{
        return accumulator + rev.rates.length;
      }, 0);
      this.dataLike.push(bookVotes);
      var gameVotes = giochi.reduce((accumulator, rev) =>{
        return accumulator + rev.rates.length;
      }, 0);
      this.dataLike.push(gameVotes);
    },
    sameArray(a, b){
      if(a.length !== b.length)
        return false;
      for(i = 0; i < a.length; i++){
        if(a[i] !== b[i])
          return false
      }
      return true
    },
    optionComposer(reviews, likes) {
      var empty = [0,0,0];
      var emptyColors = ['rgb(91,157,216)', 'rgb(117,192,76)', 'rgb(232,95,92)', '#696969'];
      var emptyLabels = ['Film/Serie TV', 'Libri', 'Giochi', 'Nessuna'];

      var baseColors = ['rgb(91,157,216)', 'rgb(117,192,76)', 'rgb(232,95,92)'];
      var baseLabels = ['Film/Serie TV', 'Libri', 'Giochi'];

      var color = baseColors;
      var label = baseLabels;
      if(this.sameArray(reviews, empty)){
        color = emptyColors;
        label = emptyLabels;
        reviews.push(1);
      }

      this.optionReview = {
       chart: {
           width: 380,
           type: 'pie',
       },
       labels: label,
       colors: color,
       fill: {
        colors: color
       },
       series: reviews,
       responsive: [{
           breakpoint: 480,
           options: {
               chart: {
                   width: 200
               },
               legend: {
                   position: 'bottom'
               }
           }
       }]
     };

     var color = baseColors;
     var label = baseLabels;
     if(this.sameArray(likes, empty)){
       color = emptyColors;
       label = emptyLabels;
       likes.push(1);
     }

     this.optionLike = {
      chart: {
          width: 380,
          type: 'pie',
      },
      labels: label,
      colors: color,
      fill: {
       colors: color
      },
      series: likes,
      responsive: [{
          breakpoint: 480,
          options: {
              chart: {
                  width: 200
              },
              legend: {
                  position: 'bottom'
              }
          }
        }]
      };
    },
  },
  filters: {
    limit: function(text, length) {
      if(text.length > length - 3)
        return text.substring(0, length-3)+"...";
      else
        return text;
    }
  }
}
