const Notify = {
  template: `
  <div class="body">
    <headerbar :key="headerKey"></headerbar>
    <main>
      <div class="main">
        <shortbar></shortbar>
        <div class="main-body">
          <!-- Notification -->
          <div class="notifications-container">

            <div class="notification-section-container">
              <div class="notification-section-title-container">
                <div class="notification-section-title">
                  Nuove
                </div>
              </div>
              <div class="notifications-SeenNotSeen">
                <div class="notification" v-for="note in Notifications.slice().reverse()" v-if="note.seen == false" @click.prevent="chooseRoute(note)">
                  <div class="notification-imgBadge-container">
                    <img :src="note.badge">
                  </div>
                  <div class="notification-description-container">
                    {{note.description}}
                  </div>
                  <div class="container-buttonJustRead" v-if="!note.seen">
                    <button class="buttonJustRead" type="button" @click.prevent="updateStatus(note)" v-on:click.stop><span class="glyphicon glyphicon-ok"></span></button>
                  </div>
                </div>
              </div>
            </div>

            <div class="notification-section-container">
              <div class="notification-section-title-container">
                <div class="notification-section-title">
                  Lette
                </div>
              </div>
              <div class="notifications-SeenNotSeen">
                <div class="notification" v-for="note in Notifications.slice().reverse().slice(0,10)" v-if="note.seen == true" @click.prevent="chooseRoute(note)">
                  <div class="notification-imgBadge-container">
                    <img :src="note.badge">
                  </div>
                  <div class="notification-description-container">
                    {{note.description}}
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- Notification End -->
        </div>
      </div>
    </main>
    <div id="push"></div>
    <footer-component></footer-component>
  </div>
  `,
  data(){
    return {
      Notifications: [],
      user: "",
      headerKey: 0,
    }
  },
  mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.resize);
    })
    this.init();
  },
  methods: {
    resize() {
      this.headerKey++;
    },
    init: function(){
      var user = sessionStorage.getItem("user_session");
      this.user = user;
      axios.get("/api/notify/notification/" + user)
      .then(result => {
        this.Notifications = [];
        result.data.forEach(note => {
          if(note.badge === "")
            note.badge = "/static/images/Badges/EvaluateBadgeNotification.png";
          this.Notifications.push(note)
        });
      });

    },
    updateStatus: function(note){
      var user = sessionStorage.getItem("user_session");
      axios.get("/api/notify/seen/" + note._id)
      .then(result => this.init());
    },
    chooseRoute(note) {
      if(note.seen === false)
        this.updateStatus(note);
      if(note.reviewId !== "")
        this.$router.push({
          name: 'review',
          params: {review: note.reviewId}
        });
      else
        this.$router.push({
          name: 'badgeBoard',
          params: {user: this.user}
        });
    },
  }
}
