Vue.component('headerbar', {
  data: function () {
    return {
      notification: [],
      activeDropdownMenu: false,
      dropDownMenuTimer: 0,
      isMobile: false,
      searchData: "",
      genre: [],
      logged: false,
      user: {
        id: "",
        username: "",
        img: ""
      },
      session: "",
      sidebarActive: false,
      sidebarWidth: "width: 0;",
      mobileSearchOpen: false
    }
  },
  mounted() {
    this.init();
  },
  created() {
    this.mediaQuery();
  },
  methods: {
    init: function() {
      this.control_session();
      window.setInterval(this.getNotifications, 5000);//5sec --//600000); /////controllo ogni 5 minuti
    },
    getNotifications: function(){
      if(this.logged){
        var user = sessionStorage.getItem("user_session");
        axios.get("/api/notify/new/" + user)
        .then(result => {
          this.notification = [];
          result.data.filter(note => note.seen == false)
          .forEach(note => this.notification.push(note));
        });
      }
    },
    mediaQuery() {
      this.isMobile = window.matchMedia('(max-width: 650px)').matches;
    },
    toggleAvatarDropdownMenu() {
      this.activeDropdownMenu = !this.activeDropdownMenu;
    },
    showAvatarDropdownMenu() {
      this.activeDropdownMenu = true;
      clearTimeout(this.dropDownMenuTimer);
    },
    hideAvatarDropdownMenuTimed() {
      this.dropDownMenuTimer = setTimeout(() => { this.activeDropdownMenu = false; }, 300);
    },
    keepAvatarDropdownMenu() {
      clearTimeout(this.dropDownMenuTimer);
      this.activeDropdownMenu = true;
    },
    hideAvatarDropdownMenu() {
      this.activeDropdownMenu = false;
    },

    ////search
    searchReviews(){
      var title = this.searchData;
      this.$router.push('/research/' + title);
    },

    ////logout
    logout: function(){
      this.logged = false;
    },

    ////user session control
    control_session(){
      var idSession = sessionStorage.getItem("user_session");
      if(idSession !== null && idSession !== undefined){
        axios. get("/api/users/" + idSession)
        .then(result => {
          this.logged = true;
          this.user.username = result.data.nickname;
          this.user.img = result.data.img;
          this.user.id = idSession;
          this.getNotifications();
        });
      }
      else this.control_Cookies();
    },

    control_Cookies(){
      var coockie = this.$cookies.get("user_session");
      if(coockie !== null && coockie !== undefined) {
        sessionStorage.setItem("user_session", coockie);
        axios. get("/api/users/" + coockie)
        .then(result => {
          this.logged = true;
          this.user.username = result.data.nickname;
          this.user.img = result.data.img;
          this.user.id = sessionStorage.getItem("user_session");
          this.getNotifications();
        });
      }
      else {
        this.logged = false;
        this.username = "";
      }
    },
    showMobileSidebar() {
      this.sidebarWidth = "width: 300px;";
      this.sidebarActive = true;
    },
    hideMobileSidebar() {
      this.sidebarWidth = "width: 0;";
      this.sidebarActive = false;
    },
    goToHome() {
      if(this.$route.fullPath != "/")
        this.$router.push("/");
    },
    goToLogin() {
      if(this.$route.fullPath != "/login")
        this.$router.push("/login");
    },
    goToProfile() {
      if(this.$route.fullPath != "/profile")
        this.$router.push("/profile");
    },
    goToAddReview() {
      if(this.logged)
        this.$router.push("/addReview");
      else  
        $('#headerModal').modal('show');
    },
    goToRankings() {
      this.$router.push("/rankings");
    },
    goToAchievements() {
      this.$router.push({name:'badgeBoard', params: {user: this.user.id}});
    },
    goToNotifications() {
      this.$router.push("/notification");
    },
    goToLogout() {
      this.$router.push("/logout");
    },
    showMobileSearch() {
      this.mobileSearchOpen = true;
    },
    hideMobileSearch() {
      this.mobileSearchOpen = false;
    }
  },
  template: `
  <!-- Header -->

  <header v-if="!isMobile">
    <div class="header-container">
      <div class="logo-container">
        <router-link to="/" title="Home">
          <img class="logo" src="/static/images/previewNew.png" alt="">
        </router-link>
      </div>
      <div class="searchBar-container">
        <input v-model="searchData" @keyup.enter="searchReviews" type="text" placeholder="Cerca" class="searchBar"/>
        <input @click.prevent="searchReviews" title="Search" value="" type="submit" class="searchButton" @keyup.enter="searchReviews"/>
      </div>
      <div class="addReviewButton-container">
        <a class="link">
          <input type="submit" value="Crea Recensione" class="headerButton headerAddReviewButton" title="Crea Recensione" @click.prevent="goToAddReview()"/>
        </a>
      </div>
      <div class="loginArea-container" v-if="logged" @click.prevent="toggleAvatarDropdownMenu">
        <div class="img-user-withNotification-container">
          <span class="img-user-withNotification">
            <mark class="big swing" v-if="notification.length > 0"/>
            <img v-if="logged" class="avatar-image" @mouseover="isMobile ? null : showAvatarDropdownMenu()" @mouseleave="isMobile ? null : hideAvatarDropdownMenuTimed()" v-bind:src="this.user.img">
          </span>
        </div>
        <div v-if="activeDropdownMenu" class="avatarDropdownMenu" @mouseover="isMobile ? null : keepAvatarDropdownMenu()" @mouseleave="isMobile ? null : hideAvatarDropdownMenu()">
          <router-link to="/profile" class="link">
            <div class="header-dropdown-line">
              <div class="header-dropdown-icon">
                <i class="fas fa-user"></i>
              </div>
              <div class="header-dropdown-text">
                Profilo
              </div>
            </div>
          </router-link>
          <hr class="line"/>
          <router-link :to="{ name:'badgeBoard', params: {user: user.id}}">
            <div class="header-dropdown-line">
              <div class="header-dropdown-icon">
                <i class="fas fa-award"></i>
              </div>
              <div class="header-dropdown-text">
                Achievements
              </div>
            </div>
          </router-link>
          <hr class="line"/>
          <router-link to="/rankings" class="link">
            <div class="header-dropdown-line">
              <div class="header-dropdown-icon">
                <i class="fas fa-trophy"></i>
              </div>
              <div class="header-dropdown-text">
                Classifica
              </div>
            </div>
          </router-link>
          <hr class="line"/>
          <router-link to="/notification" class="link">
            <div class="header-dropdown-line">
              <div class="header-dropdown-icon">
                <i class="fas fa-bell"></i>
              </div>
              <div class="notification-active" v-if="notification.length > 0">
                Notifiche 
              </div>
              <div class="red-icon" v-if="notification.length > 0">
                <i class="fa fa-circle red-icon"></i>
              </div>
              <div class="header-dropdown-text" v-else>
                Notifiche 
              </div>
            </div>
          </router-link>
          <hr class="line"/>
          <router-link to="/logout" class="link">
            <div class="header-dropdown-line">
              <div class="header-dropdown-icon">
                <i class="fas fa-sign-out-alt"></i>
              </div>
              <div class="header-dropdown-text">
                Esci
              </div>
            </div>
          </router-link>
        </div>
        <div class="nick" @mouseover="isMobile ? null : showAvatarDropdownMenu()" @mouseleave="isMobile ? null : hideAvatarDropdownMenuTimed()">
          {{this.user.username}}
        </div>
      </div>
      <div class="loginButton-container" v-if="!logged">
        <router-link to="/login" class="link">
          <input type="submit" value="Accedi" class="headerButton" title="Accedi"/>
        </router-link>
      </div>
    </div>

    <!-- Alert modal -->
    <div class="modal fade" id="headerModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Accedi</h4>
          </div>
          <div class="modal-body alert-body">
            <p>Accedi per poter pubblicare e valutare recensioni.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="profile-loginButtonMenù" data-dismiss="modal" @click.prevent="goToLogin">Accedi</button>
            <button type="button" class="profile-closeButtonMenù" data-dismiss="modal">Chiudi</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- Alert modal end -->

  </header>

  <div class="mobile-header-wrapper" v-else>
    <div class="mobile-header-container" v-if="!mobileSearchOpen">
      <div class="mobile-header-menu-button-container" @click.prevent="showMobileSidebar()">
        <i class="fa fa-bars"></i>
      </div>
      <div class="mobile-header-logo-container" @click.prevent="goToHome()">
        <img class="mobile-logo" src="/static/images/previewNew.png" alt="">
      </div>
      <div class="mobile-header-search-button-container" @click.prevent="showMobileSearch()">
        <i class="fas fa-search"></i>
      </div>
    </div>  
    
    <div class="mobile-header-container" v-else>
      <div class="mobile-header-exit-button-container" @click.prevent="hideMobileSearch()" v-if="mobileSearchOpen">
        <i class="fa fa-times"></i>
      </div>
      <div class="mobile-header-search-text-container-search" @click.prevent="goToHome()" v-if="mobileSearchOpen">
        <input v-model="searchData" @keyup.enter="searchReviews" type="text" placeholder="Cerca" class="mobile-search-bar"/>
      </div>
      <div class="mobile-header-find-button-container" @click.prevent="searchReviews()" v-if="mobileSearchOpen">
        <i class="fas fa-search"></i>
      </div>
    </div>
    
    <div class="mobile-sidebar" :style="sidebarWidth">

      <div class="mobile-sidebar-top" v-if="sidebarActive">
        <div class="mobile-sidebar-close-button-container" @click.prevent="hideMobileSidebar()">
          <i class="fas fa-times"></i>
        </div>
      </div>

      <div class="mobile-sidebar-line" @click.prevent="goToLogin()" v-if="sidebarActive && !logged" >
        <div class="mobile-sidebar-line-icon">
          <i class="fas fa-lock"></i>
        </div>
        <div class="mobile-sidebar-line-text">
          Accedi
        </div>
      </div>

      <div class="mobile-sidebar-user-line" @click.prevent="goToProfile()" v-if="sidebarActive && logged" >
        <div class="mobile-sidebar-avatar-container">
          <img class="mobile-sidebar-avatar" v-bind:src="this.user.img">
        </div>
        <div class="mobile-sidebar-nickname">
          {{user.username}} 
        </div>
      </div>

      <div class="mobile-sidebar-line" @click.prevent="goToAddReview()" v-if="sidebarActive" >
        <div class="mobile-sidebar-line-icon">
          <i class="fas fa-plus"></i>
        </div>
        <div class="mobile-sidebar-line-text">
          Crea recensione
        </div>
      </div>

      <div class="mobile-sidebar-line" @click.prevent="goToRankings()" v-if="sidebarActive && logged" >
        <div class="mobile-sidebar-line-icon">
          <i class="fas fa-trophy"></i>
        </div>
        <div class="mobile-sidebar-line-text">
          Classifica
        </div>
      </div>

      <div class="mobile-sidebar-line" @click.prevent="goToAchievements()" v-if="sidebarActive && logged" >
        <div class="mobile-sidebar-line-icon">
          <i class="fas fa-award"></i>
        </div>
        <div class="mobile-sidebar-line-text">
          Achievements
        </div>
      </div>

      <div class="mobile-sidebar-line" @click.prevent="goToNotifications()" v-if="sidebarActive && logged" >
        <div class="mobile-sidebar-line-icon">
          <i class="fas fa-bell"></i>
        </div>
        <div class="mobile-sidebar-line-text-bold" v-if="notification.length > 0">
          Notifiche 
        </div>
        <div class="red-icon" v-if="notification.length > 0">
          <i class="fa fa-circle red-icon"></i>
        </div>
        <div class="mobile-sidebar-line-text" v-else>
          Notifiche 
        </div>
      </div>

      <div class="mobile-sidebar-line" @click.prevent="goToLogout()" v-if="sidebarActive && logged" >
        <div class="mobile-sidebar-line-icon">
          <i class="fas fa-sign-out-alt"></i>
        </div>
        <div class="mobile-sidebar-line-text">
          Esci
        </div>
      </div>

    </div>
    
    <!-- Alert modal -->
    <div class="modal fade" id="headerModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Accedi</h4>
          </div>
          <div class="modal-body alert-body">
            <p>Accedi per poter pubblicare e valutare recensioni.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="profile-loginButtonMenù" data-dismiss="modal" @click.prevent="goToLogin">Accedi</button>
            <button type="button" class="profile-closeButtonMenù" data-dismiss="modal">Chiudi</button>
          </div>
        </div>
        
      </div>
    </div>
    <!-- Alert modal end -->
  </div>

  <!-- Header End -->
  `
});
