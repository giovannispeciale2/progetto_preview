Vue.component('graphic-component', {
  data: function(){
    return {
      renderedResult: [],
      dataReviews: [],
      dataLike: [],
      optionReview: {},
      optionLike: {}
    }
  },
  mounted() {
    this.init();
  },
  methods: {
    // init: function() {
    //   let promise = this.getData().then(res => {
    //     this.editorData();
    //     this.optionComposer();
    //     var chart = new ApexCharts(document.querySelector("#chart"), this.optionReview);
    //     var chart2 = new ApexCharts(document.querySelector("#chart2"), this.optionLike);
    //     chart.render();
    //     chart2.render();
    //   });
    // },
    // getData(){
    //   return new Promise( (resolve, reject) => {
    //     var userId = sessionStorage.getItem("user_session");
    //     axios.get("/api/reviews/user/" + userId)
    //     .then(response => {
    //       new Promise((res, rej) => {
    //         response.data.forEach(review => {
    //           axios.get('/api/works/' + review.work)
    //           .then(responseWork => {
    //             var result = {};
    //             result.category = responseWork.data.category;
    //             result.rates = review.rates;
    //             this.renderedResult.push(result);
    //           });
    //         });
    //         res(1);
    //       }).then(resolve(2));
    //     });
    //   });
    // },
    init: function() {
      this.getData();
      //this.editorData();
      this.optionComposer();
      var chart = new ApexCharts(document.querySelector("#chart"), this.optionReview);
      var chart2 = new ApexCharts(document.querySelector("#chart2"), this.optionLike);
      chart.render();
      chart2.render();
    },
    getData(){
      var userId = sessionStorage.getItem("user_session");
      axios.get("/api/users/updateGraphics", {params: {idU: userId}})
      .then(res => {
        console.log(res);
        this.dataReviews.push(res.data[0]);
        this.dataLike.push(res.data[1]);
      });
    },
    // editorData(){
    //   var film = this.renderedResult.filter(rev => rev.category === "Film/Serie TV");
    //   var giochi = this.renderedResult.filter(rev => rev.category === "Giochi");
    //   var libri = this.renderedResult.filter(rev => rev.category === "Libri");
    //   this.dataReviews.push(film.length);
    //   this.dataReviews.push(libri.length);
    //   this.dataReviews.push(giochi.length);
    //
    //   var movieVotes = film.reduce((accumulator, rev) =>{
    //     return accumulator + rev.rates.length;
    //   }, 0);
    //   this.dataLike.push(movieVotes);
    //   var bookVotes = libri.reduce((accumulator, rev) =>{
    //     return accumulator + rev.rates.length;
    //   }, 0);
    //   this.dataLike.push(bookVotes);
    //   var gameVotes = giochi.reduce((accumulator, rev) =>{
    //     return accumulator + rev.rates.length;
    //   }, 0);
    //   this.dataLike.push(gameVotes);
    // },
    optionComposer(){
      this.optionReview = {
       chart: {
           width: 380,
           type: 'pie',
       },
       labels: ['Film/Serie TV', 'Libri', 'Giochi'],
       colors: ['rgb(91,157,216)', 'rgb(117,192,76)', 'rgb(232,95,92)'],
       fill: {
        colors: ['rgb(91,157,216)', 'rgb(117,192,76)', 'rgb(232,95,92)']
       },
       series: this.dataReviews,
       responsive: [{
           breakpoint: 480,
           options: {
               chart: {
                   width: 200
               },
               legend: {
                   position: 'bottom'
               }
           }
       }]
     };

     this.optionLike = {
      chart: {
          width: 380,
          type: 'pie',
      },
      labels: ['Film/Serie TV', 'Libri', 'Giochi'],
      colors: ['rgb(91,157,216)', 'rgb(117,192,76)', 'rgb(232,95,92)'],
      fill: {
       colors: ['rgb(91,157,216)', 'rgb(117,192,76)', 'rgb(232,95,92)']
      },
      series: this.dataLike,
      responsive: [{
          breakpoint: 480,
          options: {
              chart: {
                  width: 200
              },
              legend: {
                  position: 'bottom'
              }
          }
      }]
    };
    }
  },
  template: `
    <div class="graphics-container">
      <div class="title-chart">
        N° Recensioni
      </div>
      <div id="chart">
        <apexchart type=pie width=380 :options="chartOptions" :series="series" />
      </div>
      <div class="title-chart">
        N° Voti
      </div>
      <div id="chart2">
        <apexchart type=pie width=380 :options="chartOptions" :series="series" />
      </div>
    </div>
  `
});
