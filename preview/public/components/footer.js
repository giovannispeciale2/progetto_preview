Vue.component('footer-component', {
    data: function () {
        return {}
    },
    template: `
        <footer>
            <div class="footer-container">
            &copy; 2019 (p)review
            </div>
        </footer>
    `
});