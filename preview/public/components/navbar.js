Vue.component('shortbar', {
    data: function () {
      return {
        categories: [],
        genre: [],
        isRank: false,




        rank: [],
        user: {
          'username': "",
          'points': 0,
          'bonus': 0,
          'position': 0
        }
      }
    },
    mounted() {
      this.init();
    },
    methods: {
      init: function() {
        this.listCategories();



        this.chargeRank();
      },
      ////categories and genres
      hideGenres: function(_id) {
        var i = this.categories.findIndex(item => item._id === _id);
        this.categories[i].showGenres = false;
      },

      listCategories: function() {
        axios.get("/api/categories")
        .then(response => this.categories = response.data)
        .then(response => {
          for(var cat in this.categories)
            cat.showGenres = false;
        })
        .catch(err => console.log(err));
      },

      setBackgroundColor: function(color) {
        return "background: " + color + ";";
      },






      getColor(user){
        var index = this.rank.findIndex(u => u.nickname === user.nickname);
        var color = {
          'even': "background-color: white",
          'odd': "background-color: #f0f7f6",
          'logged': "background-color: #7df779",
        };
        if (user._id == sessionStorage.getItem("user_session"))
          return color.logged;
        else if (index % 2 == 0)
          return color.even;
        else
          return color.odd;
      },
      getPosition(user){
        return this.rank.findIndex(u => u.nickname === user.nickname) + 1;
      },
      getUser() {
        var userId = sessionStorage.getItem("user_session");
        axios.get("/api/users/" + userId)
        .then(result => {
          this.user.username = result.data.nickname;
          this.user.points = result.data.points;
          this.user.bonus = result.data.bonus;
          this.user.position = this.rank.findIndex(u => u.nickname === result.data.nickname);
        });
      },
      showRank() {
        return this.$router.currentRoute.fullPath != "/rankings";
      },
      chargeRank() {
        axios.get("/api/userRanking")
        .then(result => {
          result.data.forEach(user => {
            this.rank.push(user);
          });
          this.getUser();
        });
      },
      goToRankings() {
        this.$router.push('/rankings');
      },
      goToProfile(result) {
        this.$router.push({ name:'others', params: {username: result.nickname}});
      },
    },
    template: `
    <!-- SideNav -->
    <nav>
      <ul class="category-list">
        <li class="category" v-for="category in categories" :key="categories._id" v-bind:style="setBackgroundColor(category.color)">
          <div class="category-container" v-bind:title="category.name">
            <router-link :to="{ name:'categories', params: {categoria: category.name}}">
              <div class="category-link-container">
                <div class="category-icon">
                  <i class="material-icons">{{category.img}}</i>
                </div>
                <div class="category-name">
                  {{category.name}}
                </div>
              </div>
            </router-link>
          </div>
          <ul class="genre-list" v-bind:style="setBackgroundColor(category.color)">
            <li v-for="genre in category.genres" v-bind:title="genre">
              <router-link :to="{ name:'genres', params: {categoria: category.name, genere: genre}}">
                <div class="genre-name">
                  {{genre}}
                </div>
              </router-link>
              <hr class="genre-line" v-if="category.genres.findIndex(g => genre === g) != category.genres.length - 1"/>
            </li>
          </ul>
        </li>
      </ul>



      <!-- Rankings -->
      <div class="ranking ranking-nav" v-if="showRank()">
        <table class="ranking-users-nav">
          <tr class="ranking-users-rowHead">
            <th colspan="3" @click.prevent='goToRankings'>
              <span class="ranking-icon"><i class="fas fa-trophy"></i></span> Classifica
            </th>
          </tr>
          <tr class="ranking-users-row ranking-users-row-nav" v-for= "result in rank.slice(0, 10)" v-bind:style= "getColor(result)" @click.prevent="goToProfile(result)">
            <td class="ranking-position"> {{getPosition(result)}} </td>
            <td class="ranking-nick">
              <!--<router-link :to="{ name:'others', params: {username: result.nickname}}">-->
                {{result.nickname}}
              <!--</router-link>-->
            </td>
            <td class="ranking-points-total">{{result.points}}</td>
          </tr>
        </table>
      </div>
      <!-- Rankings End -->
    </nav>
    <!-- SideNav End -->
    `
});
