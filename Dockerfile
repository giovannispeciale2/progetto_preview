FROM node

RUN mkdir preview

COPY ./preview /preview

RUN cd /preview ; npm install

CMD cd /preview ; node app.js